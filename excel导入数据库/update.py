#This Python file uses the following encoding: utf-8
'''
Created on 2014-6-5

@author: fwang
'''
import sys,os
currPath=os.path.abspath(".").replace("\\","/")
sys.path.insert(0,currPath)

import xlrd

def del_all():
    files=open("del_all.sql",'w')
    date="delete from course;"
    date+="delete from course_unit_price;"
    files.writelines(date)
    files.close()
    os.system("mysql -u root food_db< del_all.sql")
    os.system("del del_all.sql")
def insert(number,name,ab,types,unit,price):
    files=open("update.sql",'w')
    date= "insert into course(restaurant_id,number,name,name_en,ab,type)"
    date+="values(1,'"+number+"','"+name+"','','"+ab+"','"+types+"');\r\n"
    date+="insert into course_unit_price(restaurant_id,course_id,type,unit,price1,"
    date+="is_active,print_type,rebate_percent_max,rebate_price_max,printer,"
    date+="weight_printer,hasten_printer,spare_printer,print_page,price2,price3,price4,price5,price6,price7,price8,"
    date+="sale_unit,is_serial,is_modif_price,is_dish,is_stripe)"
    date+="values(1,(SELECT max(id) FROM course),'"+types+"','"+unit+"',"+price+",1,"
    date+="'',0.0,0.0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0,0,0);\r\n"
    #SELECT max(id) FROM course;
    print date
    files.write(date.encode('gbk'))
    files.close()
    os.system("mysql -u root food_db< update.sql")
def main():
    data = xlrd.open_workbook('food.xls')
    table = data.sheets()[0] 
    nrows = table.nrows
    #nrows=2
    for i in range(nrows):
        #print table.row_values(i)
        if i==0:
            continue
        cell_A1 = (table.cell(i,0).value)
        cell_A2 = (table.cell(i,1).value)
        cell_A3 = (table.cell(i,2).value)
        cell_A4 = (table.cell(i,3).value)
        cell_A5 = (table.cell(i,4).value)
        cell_A6 = str(table.cell(i,5).value)
        insert(cell_A1,cell_A2,cell_A4,cell_A3,cell_A5,cell_A6)
        #print cell_A1.encode("utf-8")+' '+cell_A2.encode('utf-8')+' '+\
       # cell_A3.encode("utf-8")+' '+cell_A4.encode('utf-8')+' '+\
       # cell_A5.encode("utf-8")+' '+cell_A6.encode('utf-8')
        
    return
if __name__ == '__main__':
    del_all()
    main()
    os.system("del update.sql")
    
    
