-- MySQL dump 10.13  Distrib 6.0.6-alpha, for Win32 (ia32)
--
-- Host: localhost    Database: food_db
-- ------------------------------------------------------
-- Server version	6.0.4-alpha-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cancel_cause`
--

DROP TABLE IF EXISTS `cancel_cause`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cancel_cause` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cancel_cause`
--

LOCK TABLES `cancel_cause` WRITE;
/*!40000 ALTER TABLE `cancel_cause` DISABLE KEYS */;
INSERT INTO `cancel_cause` VALUES (1,'1','²ËÆ·ÖÊÁ¿');
/*!40000 ALTER TABLE `cancel_cause` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(10) unsigned NOT NULL,
  `number` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `name_en` varchar(64) NOT NULL,
  `ab` varchar(64) NOT NULL,
  `type` varchar(64) NOT NULL,
  `is_sellout` int(11) DEFAULT '0',
  `sellout_quantity` double DEFAULT '0',
  `sellout_cause` date DEFAULT '2008-11-25',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (2,1,'10002','´ÖÁ¸ÉÈ×Ó¹Ç','zlszg','zlszg','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (3,1,'10003','Îå¹È·áµÇ32','wgfd','dwgf','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (51,1,'10001','ÀÏÑ¼ìÒ88Ôª','lyb','lyb','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (52,1,'10004','¸ÉÕôÍÁ¼¦','gztj','gztj','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (53,1,'10005','Å´Ã×ÅÅ¹Ç','nmpg','nmpg','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (54,1,'10006','¼ªÏéÈý±¦','','jxsb','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (55,1,'10007','À°¼¦¿Û½ðÇ®','ljkjq','ljkjq','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (56,1,'10008','¼«Æ·±¾¼¦','jpbj','jpbj','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (57,1,'10009','É½¾úìÀÈé¸ë','sjdrg','sjdrg','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (58,1,'10010','ÎÄ¸òìÀµ°','wgdd','wgdd','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (59,1,'10011','ÏÌÅÅ·ï×¦','xpfz','xpfz','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (60,1,'10012','ÇåÕô°×Óã','qzby','qzby','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (61,1,'10013','ÏÌÈâÕôÇ§ÕÅ','xrzqz','xrzqz','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (62,1,'10014','Õæ¹¦·ò±«Óã','zgfby','zgfby','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (63,1,'10015','ÃØÖÆÏÌ»ÆÓã','mzxhy','mzxhy','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (64,1,'10016','¸ÉÕô¼×Óã','gzjy','gzjy','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (65,1,'10017','»Æ½ðÓãÍ·','hjyt','hjyt','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (66,1,'10018','É°¹øõtÓã','sghy','sghy','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (68,1,'10020','Ê¯¹ø²èÊ÷¹½','sgcsg','sgcsg','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (69,1,'10021','Ð··ÛÊ¨×ÓÍ·','xfszt','xfszt','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (70,1,'20001','½ðÅÆÅ£Á¦¹Ç','jpnlg','jpnlg','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (71,1,'20002','ÉÕÎ¶Æ´ÅÌ','swpp','swpp','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (72,1,'20003','¹ãÊ½ÉÕ¶ì','gsse','gsse','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (73,1,'20004','ÃÛÖ­²æÉÕ','mzcs','mzcs','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (74,1,'20005','°ÄÃÅÉÕÈâ','amsr','amsr','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (75,1,'20006','ÃØÖÆÌ¿ÉÕÈâ','mztsr','mztsr','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (76,1,'20007','Â±Ë®Æ´ÅÌ','lspp','lspp','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (77,1,'20008','ºÉÏãÉñÏÉ¼¦','hxsxj','hxsxj','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (78,1,'20009','º£²ÎÓã¶Ç¸þ','hsydg','hsydg','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (79,1,'20010','¹¬Í¢ËÄ±¦¸þ','gtsbg','gtsbg','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (80,1,'20011','ÑÎŸh¼¦°ëÖ»','yjj','yjj','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (81,1,'20012','ÑÎŸh·ï×¦','yjfz','yjfz','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (83,1,'20013','¹ãÊ½¹¦·òÌÀ','gsgft','gsgft','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (84,1,'20014','¹¦·òÒ©ÉÅìÀÈé¸ë','gfysdrg','gfysdrg','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (85,1,'20015','ÌØ¼ÛÓã³á¸þ','tjycg','tjycg','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (86,1,'20016','¹Ù¸®Èý±¦','gfsb','gfsb','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (87,1,'20017','»ÆìËº£²ÎË¿','hmhss','hmhss','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (88,1,'20018','³áÌÀÑþÖù¸É±«×Ð','ctyzgbz','ctyzgbz','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (89,1,'20019','ÌØ¼Û¸ßÌÀ·ðÌøÇ½','tjgtftq','tjgtftq','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (90,1,'20020','±«Ö­»¨¹½¶ìÕÆ','bzhgez','bzhgez','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (91,1,'20021','Ä¾¹ÏìÀÑ©¸ò','mgdxh','mgdxh','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (92,1,'20022','ÔÓÁ¸ÁÉ²Î','zlls','zlls','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (93,1,'20023','ËÉÈ×ìÀÁÉ²Î','srdls','srdls','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (94,1,'20024','ÉúÊÈÌìÄ¿ºþÓãÍ·','sstmhyt','sstmhyt','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (95,1,'20025','ÉúÊÈÅ£ÍÜ','ssnw','ssnw','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (96,1,'20026','ÉúÊÈË«´à','sssc','sssc','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (97,1,'20027','ÉúÊÈ¹ã¶«ÇåÔ¶¼¦','ssgdqyj','ssgdqyj','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (98,1,'30001','°ÄÖÞÁúÏº','azlx','azlx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (99,1,'30002','´óÁ¬±«','dlb','dlb','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (100,1,'30003','Õä±¦Ð·','zbx','zbx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (101,1,'30004','ºÓÏº','hx','hx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (102,1,'30005','°×Ïº','bx','bx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (103,1,'30006','°ß½ÚÏº','bjx','bjx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (104,1,'30007','»ùÎ§Ïº','jwx','jwx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (105,1,'30008','²¨Ê¿¶ÙÁúÏº','bsdlx','bsdlx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (106,1,'30009','»¨Ð·','hx','hx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (107,1,'30010','ÖñòÉ»Ê','zch','zch','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (108,1,'30011','ÎÄ¸ò','wh','wh','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (109,1,'30012','¹ðÓã','gy','gy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (110,1,'30013','õtÓã','hy','hy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (111,1,'30014','öÔÓã','ly','ly','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (113,1,'30015','Ð¡°ÍÓã','xby','xby','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (114,1,'30016','¼×Óã','jy','jy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (115,1,'30017','öêÓã','jy','jy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (116,1,'30018','»Æ÷­','hs','hs','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (117,1,'30019','ÖÐ»ªöà','zhx','zhx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (118,1,'30020','Ò°Éú´óöêÓã','ysdjy','ysdjy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (119,1,'30021','ÎÌ¹«Óã','wgy','wgy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (120,1,'30022','Ò°Éú´óöýÓã','ysdby','ysdby','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (121,1,'30023','¶à±¦Óã','dby','dby','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (122,1,'30024','Ò°Éú´ó¼×Óã','ysdjy','ysdjy','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (123,1,'40001','ÐÓ±«¹½³´ºÚÓãÆ¬','xbgchyp','xbgchyp','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (124,1,'40002','²Ý¹½ÏÊ±´','cgxb','cgxb','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (126,1,'40003','²èÊ÷¹½ËÉÚæÈâ','csgsbr','csgsbr','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (127,1,'40004','ºÚ½·Å£×Ð¹Ç','hjnzg','hjnzg','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (128,1,'40005','´óÖó¸ÉË¿','dzgs','dzgs','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (129,1,'40006','¼¦Ö­Ëñ¸É','jzsg','jzsg','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (130,1,'40007','Ôª±¦Ïº','ybx','ybx','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (131,1,'40008','ºÓ°öÏÌ¼¦ìÐÂÜ²·','hbxjwlb','hbxjwlb','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (132,1,'40009','¶¥ÌÀ¶¹¸¯','dtdf','dtdf','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (133,1,'40010','¼Ò³£ìËµ°','jcmd','jcmd','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (134,1,'40011','¾©½´¼¦Áø','jjjl','jjjl','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (135,1,'40012','ÕÇµ°ÏÊ','zdx','zdx','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (136,1,'40013','ÃÀÊ½Å£ÈâÁ£','msnrl','msnrl','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (137,1,'40014','Å©¼Ò´óÓãÍ·','njdyt','njdyt','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (138,1,'40015','Å¨Çé·ÊÅ£','nqfn','nqfn','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (139,1,'40016','ÈâÄðÇ§ÕÅ½á','rnqzj','rnqzj','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (140,1,'40017','Èý¹½ÓãÆ¬','sgyp','sgyp','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (141,1,'40018','ÉÏÌÀË¿ÂÝ','stsl','stsl','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (142,1,'40019','ÊÖËº°ü²Ë','ssbc','ssbc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (143,1,'40020','Ë®¾§ÏºÈÊ','sjxr','sjxr','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (144,1,'40021','Ìú°å¶¹¸¯','tbdf','tbdf','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (145,1,'40022','ÏìÓÍ÷­ºý','xysh','xysh','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (146,1,'40023','Ð··Û¶¹¸¯','xfdf','xfdf','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (147,1,'40024','Ð··ÛÓã³á','xfyc','xfyc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (148,1,'40025','Ð··ÛÌã½î','xftj','xftj','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (149,1,'40026','Òâ´óÀû¿¾Èâ','ydlkr','ydlkr','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (150,1,'40027','Ê²½õÃæ½î','sjmj','sjmj','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (151,1,'40028','´óÌÀº£´ø','dthd','dthd','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (152,1,'40029','¶¥ÌÀÓÍÔüÂÜ²·','dtyzlb','dtyzlb','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (153,1,'40030','½ðÕë²ËÉÕËØ¼¦','jzcssj','jzcssj','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (154,1,'40031','³áÌÀ¹ðÓã','ctgy','ctgy','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (155,1,'40032','·çÎ¶Ñ¼Ñª','fwyx','fwyx','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (156,1,'40033','·ÐÌÚÓã','fty','fty','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (157,1,'40034','ÂéÆÅ¶¹¸¯','mpdf','mpdf','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (158,1,'40035','Ëá²ËÓã','scy','scy','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (159,1,'40036','Ë®ÖóÈý¹ú','szsg','szsg','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (160,1,'40037','¸É¹øÅ£Èâ','ggnr','ggnr','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (161,1,'40038','ÓãÏãÈâË¿','yxrs','yxrs','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (162,1,'40039','Çà½·ÍÁ¶¹Ë¿','qjtds','qjtds','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (163,1,'40040','²¤²Ë','bc','bc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (164,1,'40041','¾Â²Ë','jc','jc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (165,1,'40042','ÍÞÍÞ²Ë','wwc','wwc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (166,1,'40043','ÇÑ×Ó','qz','qz','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (167,1,'40044','Éú²Ë','sc','sc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (168,1,'40045','¹ã¶«²ËÐÄ','gdcx','gdcx','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (169,1,'40046','Î÷ÇÛ°ÙºÏ','xqbh','xqbh','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (170,1,'40047','Î÷À¼»¨','xlh','xlh','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (171,1,'40048','¶ç½·ÓãÍ·','djyt','djyt','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (172,1,'50001','ÇØºþÒ°¼¦µ°','qhyjd','qhyjd','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (173,1,'50002','ÌØÉ«ÂÜ²·Æ¤','tslbp','tslbp','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (174,1,'50003','´ÐÓÍ²Ï¶¹','cycd','cycd','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (175,1,'50004','Ïã¸ÉÂíÀ¼','xgml','xgml','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (176,1,'50005','Ò»Æ·´àÈé¹Ï','ypcrg','ypcrg','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (177,1,'50006','Êß²ËÉ«À­','scsl','scsl','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (178,1,'50007','ÈâÖ­Ð¡Ïã¹½','rzxxg','rzxxg','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (179,1,'50008','´×½·º£´ø»¨','cjhdh','cjhdh','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (180,1,'50009','Çà¹ÏÆ¤µ°','qgpd','qgpd','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (181,1,'50010','²Ø±¦ÄÏ¹Ï','cbng','cbng','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (182,1,'50011','Õº½´»Æ¹Ï','zjhg','zjhg','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (183,1,'50012','Ë¬¿ÚÍÞÍÞ²Ë','skwwc','skwwc','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (184,1,'50013','´à¿ÚÉ½Ò©','cksy','cksy','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (185,1,'50014','¹ð»¨ÌÇÅº','ghto','ghto','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (187,1,'50015','¼ªÏéÈý±¦Àä²Ë','jxsb','jxsb','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (188,1,'50016','Á¹°èÝ«ËñË¿','lbwss','lbwss','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (189,1,'50017','Ì©Ê½ËáÀ±·ï×¦','tsslfz','tsslfz','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (190,1,'50018','Ê²½õÀÏÌ³×Ó','sjltz','sjltz','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (191,1,'50019','ÑÎË®Èé¸ë','ysrg','ysrg','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (192,1,'50020','µ°Ãæ°×Èâ','dmbr','dmbr','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (193,1,'50021','ÀÏÉÏº£Ñ¬Óã','lshxy','lshxy','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (194,1,'50022','Ñ©Ö­»ÆÅ£Èâ','xzhnr','xzhnr','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (195,1,'50023','¹Å·¨×íÏãÂÝ','gfzxl','gfzxl','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (196,1,'50024','¼«Æ·º£òØÍ·','jphzt','jphzt','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (199,1,'50025','Ë¬¿Ú´àÆ¤¼¦','skcpj','skcpj','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (200,1,'50026','°×ÇÐ¶Ç¼â','bqdj','bqdj','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (201,1,'50027','Ïã¼åÂíÍ·Óã','xjmty','xjmty','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (202,1,'50028','ÌØÉ«Å£Í·Æ¤','tsntp','tsntp','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (203,1,'50029','ÃØÖÆÊì×íÐ·','mzszx','mzszx','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (204,1,'50030','ÉÜÐË×í×Ð¶ì','sxzze','sxzze','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (205,1,'50031','Ïã¿¾Î¶àáÓã','xkwcy','xkwcy','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (206,1,'50032','ÉÏº£Èý»Æ¼¦','shshj','shshj','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (207,1,'10022','ÁÙÊ±²Ë´óÁý','lscdl','lscdl','ÕôÁýìÒ×Ð',0,0,'2008-11-25');
INSERT INTO `course` VALUES (208,1,'20028','ÁÙÊ±²ËÉÕÀ°','lscsl','lscsl','ÉÕÀ°',0,0,'2008-11-25');
INSERT INTO `course` VALUES (209,1,'30025','ÁÙÊ±²Ëº£ÏÊ','lschx','lschx','ºþº£ÏÊ',0,0,'2008-11-25');
INSERT INTO `course` VALUES (210,1,'40049','ÁÙÊ±²ËÈÈ²Ë','lscrc','lscrc','ÈÈ²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (211,1,'50033','ÁÙÊ±²ËÀä²Ë','lsclc','lsclc','Àä²Ë',0,0,'2008-11-25');
INSERT INTO `course` VALUES (212,1,'888','Ñç»áÌ×²Í','yhtc','yhtc','ÈÈ²Ë',0,0,'2008-11-25');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_operate_log`
--

DROP TABLE IF EXISTS `course_operate_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_operate_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `or_id` int(11) NOT NULL,
  `or_name` varchar(32) DEFAULT NULL,
  `or_item_id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `modifed` double NOT NULL,
  `percentage` double DEFAULT NULL,
  `cause` varchar(64) NOT NULL,
  `authorize` varchar(64) NOT NULL,
  `operators` varchar(64) NOT NULL,
  `operators_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_operate_log`
--

LOCK TABLES `course_operate_log` WRITE;
/*!40000 ALTER TABLE `course_operate_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_operate_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_remark`
--

DROP TABLE IF EXISTS `course_remark`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `remark_id` int(10) unsigned NOT NULL,
  `account_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_remark`
--

LOCK TABLES `course_remark` WRITE;
/*!40000 ALTER TABLE `course_remark` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_remark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_serial`
--

DROP TABLE IF EXISTS `course_serial`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_serial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(10) unsigned NOT NULL,
  `number` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `name_en` varchar(64) NOT NULL,
  `ab` varchar(64) NOT NULL,
  `unit` varchar(8) NOT NULL,
  `price1` double DEFAULT NULL,
  `price2` double DEFAULT NULL,
  `price3` double DEFAULT NULL,
  `price4` double DEFAULT NULL,
  `price5` double DEFAULT NULL,
  `price6` double DEFAULT NULL,
  `price7` double DEFAULT NULL,
  `price8` double DEFAULT NULL,
  `really_price` double NOT NULL,
  `is_modif_price` tinyint(1) DEFAULT NULL,
  `type` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_serial`
--

LOCK TABLES `course_serial` WRITE;
/*!40000 ALTER TABLE `course_serial` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_serial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_serial_item`
--

DROP TABLE IF EXISTS `course_serial_item`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_serial_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `count` double DEFAULT '1',
  `is_switch` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_serial_item`
--

LOCK TABLES `course_serial_item` WRITE;
/*!40000 ALTER TABLE `course_serial_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_serial_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `course_serial_item_view`
--

DROP TABLE IF EXISTS `course_serial_item_view`;
/*!50001 DROP VIEW IF EXISTS `course_serial_item_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `course_serial_item_view` (
  `serial_id` int(10) unsigned,
  `is_switch` int(11),
  `course_id` int(10) unsigned,
  `count` double,
  `number` varchar(16),
  `name` varchar(64),
  `name_en` varchar(64),
  `ab` varchar(64),
  `type` varchar(64),
  `dept` varchar(64),
  `price1` double,
  `price2` double,
  `price3` double,
  `price4` double,
  `price5` double,
  `price6` double,
  `price7` double,
  `price8` double,
  `unit` varchar(8),
  `sale_unit` varchar(8),
  `is_sellout` int(11),
  `is_serial` tinyint(1),
  `is_modif_price` tinyint(1),
  `is_dish` tinyint(1),
  `sellout_cause` date,
  `sellout_quantity` double
) */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `course_sort`
--

DROP TABLE IF EXISTS `course_sort`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `sort_item_id` int(10) unsigned NOT NULL,
  `sort_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_sort`
--

LOCK TABLES `course_sort` WRITE;
/*!40000 ALTER TABLE `course_sort` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_sort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_unit_price`
--

DROP TABLE IF EXISTS `course_unit_price`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `course_unit_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `type` varchar(64) NOT NULL,
  `print_type` varchar(64) NOT NULL,
  `recommend_type` varchar(64) DEFAULT NULL,
  `unit` varchar(8) NOT NULL,
  `price1` double DEFAULT NULL,
  `price2` double DEFAULT NULL,
  `price3` double DEFAULT NULL,
  `price4` double DEFAULT NULL,
  `price5` double DEFAULT NULL,
  `price6` double DEFAULT NULL,
  `price7` double DEFAULT NULL,
  `price8` double DEFAULT NULL,
  `sale_unit` varchar(8) DEFAULT NULL,
  `rebate_percent_max` double NOT NULL,
  `rebate_price_max` double NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_serial` tinyint(1) DEFAULT NULL,
  `is_modif_price` tinyint(1) DEFAULT NULL,
  `is_dish` tinyint(1) DEFAULT NULL,
  `is_stripe` tinyint(1) DEFAULT NULL,
  `printer` int(10) unsigned NOT NULL,
  `weight_printer` int(10) unsigned NOT NULL,
  `hasten_printer` int(10) unsigned NOT NULL,
  `spare_printer` int(10) unsigned NOT NULL,
  `print_page` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `course_unit_price`
--

LOCK TABLES `course_unit_price` WRITE;
/*!40000 ALTER TABLE `course_unit_price` DISABLE KEYS */;
INSERT INTO `course_unit_price` VALUES (2,1,2,'ÕôÁýìÒ×Ð','',NULL,'·Ý',52,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (3,1,3,'ÕôÁýìÒ×Ð','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (51,1,51,'ÕôÁýìÒ×Ð','',NULL,'·Ý',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (52,1,52,'ÕôÁýìÒ×Ð','',NULL,'·Ý',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (53,1,53,'ÕôÁýìÒ×Ð','',NULL,'·Ý',42,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (54,1,54,'ÕôÁýìÒ×Ð','',NULL,'·Ý',36,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (55,1,55,'ÕôÁýìÒ×Ð','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (56,1,56,'ÕôÁýìÒ×Ð','',NULL,'·Ý',128,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (57,1,57,'ÕôÁýìÒ×Ð','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (58,1,58,'ÕôÁýìÒ×Ð','',NULL,'·Ý',25,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (59,1,59,'ÕôÁýìÒ×Ð','',NULL,'·Ý',42,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (60,1,60,'ÕôÁýìÒ×Ð','',NULL,'·Ý',888,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (61,1,61,'ÕôÁýìÒ×Ð','',NULL,'·Ý',35,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (62,1,62,'ÕôÁýìÒ×Ð','',NULL,'Î»',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (63,1,63,'ÕôÁýìÒ×Ð','',NULL,'·Ý',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (64,1,64,'ÕôÁýìÒ×Ð','',NULL,'·Ý',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (65,1,65,'ÕôÁýìÒ×Ð','',NULL,'·Ý',78,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (66,1,66,'ÕôÁýìÒ×Ð','',NULL,'·Ý',76,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (68,1,68,'ÕôÁýìÒ×Ð','',NULL,'·Ý',48,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (69,1,69,'ÕôÁýìÒ×Ð','',NULL,'Ö»',9,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (70,1,70,'ÉÕÀ°','',NULL,'Ö»',268,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (71,1,71,'ÉÕÀ°','',NULL,'·Ý',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (72,1,72,'ÉÕÀ°','',NULL,'·Ý',78,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (73,1,73,'ÉÕÀ°','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (74,1,74,'ÉÕÀ°','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (75,1,75,'ÉÕÀ°','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (76,1,76,'ÉÕÀ°','',NULL,'·Ý',78,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (77,1,77,'ÉÕÀ°','',NULL,'·Ý',128,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (78,1,78,'ÉÕÀ°','',NULL,'·Ý',138,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (79,1,79,'ÉÕÀ°','',NULL,'·Ý',108,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (80,1,80,'ÉÕÀ°','',NULL,'Ö»',58,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (81,1,81,'ÉÕÀ°','',NULL,'·Ý',48,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (82,1,83,'ÉÕÀ°','',NULL,'Î»',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (83,1,84,'ÉÕÀ°','',NULL,'Î»',36,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (84,1,85,'ÉÕÀ°','',NULL,'Î»',36,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (85,1,86,'ÉÕÀ°','',NULL,'Î»',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (86,1,87,'ÉÕÀ°','',NULL,'Î»',30,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (87,1,88,'ÉÕÀ°','',NULL,'Î»',30,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (88,1,89,'ÉÕÀ°','',NULL,'Î»',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (89,1,90,'ÉÕÀ°','',NULL,'Ö»',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (90,1,91,'ÉÕÀ°','',NULL,'Î»',48,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (91,1,92,'ÉÕÀ°','',NULL,'Î»',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (92,1,93,'ÉÕÀ°','',NULL,'Î»',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (93,1,94,'ÉÕÀ°','',NULL,'·Ý',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (94,1,95,'ÉÕÀ°','',NULL,'·Ý',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (95,1,96,'ÉÕÀ°','',NULL,'·Ý',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (96,1,97,'ÉÕÀ°','',NULL,'Ö»',108,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (97,1,98,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (98,1,99,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (99,1,100,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (100,1,101,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (101,1,102,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (102,1,103,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (103,1,104,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (104,1,105,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (105,1,106,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (106,1,107,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,0);
INSERT INTO `course_unit_price` VALUES (107,1,108,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (108,1,109,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (109,1,110,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (110,1,111,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (111,1,113,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (112,1,114,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (113,1,115,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (114,1,116,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (115,1,117,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (116,1,118,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (117,1,119,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (118,1,120,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (119,1,121,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (120,1,122,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (121,1,123,'ÈÈ²Ë','',NULL,'·Ý',48,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (122,1,124,'ÈÈ²Ë','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (123,1,126,'ÈÈ²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (124,1,127,'ÈÈ²Ë','',NULL,'·Ý',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (125,1,128,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (126,1,129,'ÈÈ²Ë','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (127,1,130,'ÈÈ²Ë','',NULL,'·Ý',58,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (128,1,131,'ÈÈ²Ë','',NULL,'·Ý',52,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (129,1,132,'ÈÈ²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (130,1,133,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (131,1,134,'ÈÈ²Ë','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (132,1,135,'ÈÈ²Ë','',NULL,'·Ý',36,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (133,1,136,'ÈÈ²Ë','',NULL,'·Ý',42,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (134,1,137,'ÈÈ²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (135,1,138,'ÈÈ²Ë','',NULL,'·Ý',48,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (136,1,139,'ÈÈ²Ë','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (137,1,140,'ÈÈ²Ë','',NULL,'·Ý',58,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (138,1,141,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (139,1,142,'ÈÈ²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (140,1,143,'ÈÈ²Ë','',NULL,'·Ý',88,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (141,1,144,'ÈÈ²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (142,1,145,'ÈÈ²Ë','',NULL,'·Ý',58,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (143,1,146,'ÈÈ²Ë','',NULL,'·Ý',52,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (144,1,147,'ÈÈ²Ë','',NULL,'·Ý',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (145,1,148,'ÈÈ²Ë','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (146,1,149,'ÈÈ²Ë','',NULL,'·Ý',42,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (147,1,150,'ÈÈ²Ë','',NULL,'·Ý',25,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (148,1,151,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (149,1,152,'ÈÈ²Ë','',NULL,'·Ý',9,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (150,1,153,'ÈÈ²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (151,1,154,'ÈÈ²Ë','',NULL,'·Ý',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (152,1,155,'ÈÈ²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (153,1,156,'ÈÈ²Ë','',NULL,'·Ý',56,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (154,1,157,'ÈÈ²Ë','',NULL,'·Ý',9,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (155,1,158,'ÈÈ²Ë','',NULL,'·Ý',56,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (156,1,159,'ÈÈ²Ë','',NULL,'·Ý',56,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (157,1,160,'ÈÈ²Ë','',NULL,'·Ý',68,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (158,1,161,'ÈÈ²Ë','',NULL,'·Ý',26,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (159,1,162,'ÈÈ²Ë','',NULL,'·Ý',15,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (160,1,163,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (161,1,164,'ÈÈ²Ë','',NULL,'·Ý',15,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (162,1,165,'ÈÈ²Ë','',NULL,'·Ý',23,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (163,1,166,'ÈÈ²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (164,1,167,'ÈÈ²Ë','',NULL,'·Ý',15,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (165,1,168,'ÈÈ²Ë','',NULL,'·Ý',20,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (166,1,169,'ÈÈ²Ë','',NULL,'·Ý',25,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (167,1,170,'ÈÈ²Ë','',NULL,'·Ý',25,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (168,1,171,'ÈÈ²Ë','',NULL,'·Ý',78,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (169,1,172,'Àä²Ë','',NULL,'·Ý',12,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (170,1,173,'Àä²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (171,1,174,'Àä²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (172,1,175,'Àä²Ë','',NULL,'·Ý',16,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (173,1,176,'Àä²Ë','',NULL,'·Ý',20,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (174,1,177,'Àä²Ë','',NULL,'·Ý',20,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (175,1,178,'Àä²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (176,1,179,'Àä²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (177,1,180,'Àä²Ë','',NULL,'·Ý',20,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (178,1,181,'Àä²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (179,1,182,'Àä²Ë','',NULL,'·Ý',12,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (180,1,183,'Àä²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (181,1,184,'Àä²Ë','',NULL,'·Ý',22,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (182,1,185,'Àä²Ë','',NULL,'·Ý',20,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (183,1,187,'Àä²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (184,1,188,'Àä²Ë','',NULL,'·Ý',18,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (185,1,189,'Àä²Ë','',NULL,'·Ý',26,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (186,1,190,'Àä²Ë','',NULL,'·Ý',26,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (187,1,191,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (188,1,192,'Àä²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (189,1,193,'Àä²Ë','',NULL,'·Ý',36,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (190,1,194,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (191,1,195,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (192,1,196,'Àä²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (193,1,199,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (194,1,200,'Àä²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (195,1,201,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (196,1,202,'Àä²Ë','',NULL,'·Ý',38,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (197,1,203,'Àä²Ë','',NULL,'·Ý',35,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (198,1,204,'Àä²Ë','',NULL,'·Ý',32,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (199,1,205,'Àä²Ë','',NULL,'·Ý',98,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (200,1,206,'Àä²Ë','',NULL,'·Ý',28,0,0,0,0,0,0,0,'',0,0,1,0,0,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (201,1,207,'ÕôÁýìÒ×Ð','',NULL,'Ö»',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,7,0,7,0,1);
INSERT INTO `course_unit_price` VALUES (202,1,208,'ÉÕÀ°','',NULL,'Ö»',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,8,0,8,0,1);
INSERT INTO `course_unit_price` VALUES (203,1,209,'ºþº£ÏÊ','',NULL,'½ï',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,3,0,3,0,1);
INSERT INTO `course_unit_price` VALUES (204,1,210,'ÈÈ²Ë','',NULL,'·Ý',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,5,0,5,0,1);
INSERT INTO `course_unit_price` VALUES (205,1,211,'Àä²Ë','',NULL,'·Ý',999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,4,0,4,0,1);
INSERT INTO `course_unit_price` VALUES (206,1,212,'ÈÈ²Ë','',NULL,'·Ý',9999,0,0,0,0,0,0,0,'',0,0,1,0,1,0,0,7,0,7,0,0);
/*!40000 ALTER TABLE `course_unit_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(10) unsigned NOT NULL,
  `number` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `position` varchar(32) DEFAULT NULL,
  `dept` varchar(64) DEFAULT NULL,
  `useful_life` date DEFAULT NULL,
  `rebate_percent_max` double NOT NULL,
  `rebate_price_max` double NOT NULL,
  `erase` int(10) unsigned DEFAULT NULL,
  `purview_pos` varchar(256) NOT NULL DEFAULT '0',
  `deduct` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (151,1,'000885','740416','²Ü½¨¾ü','Íø¹Ü','Íø¹Ü','2020-01-01',0,0,2,'1111011001111111111111111111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (168,1,'1003','3311','ËïÓîºì','¾­Àí','Ç°Ìü','2009-04-20',0,0,2,'1100000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (193,1,'1328','9855','ÁõÐãÀö','°üÏá','°üÏá','2009-04-20',0,0,2,'1100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (204,1,'1339','9327','ÀîÔÃ','·þÎñÔ±','²ÍÌü','2009-04-20',0,0,2,'1100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (337,1,'080507','870914','ÈÎÞÈ½à','ÊÕÒø','ÊÕÒø','2080-04-20',75,200,2,'1111110111111000111110110010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (338,1,'885','885','²Ü½¨¾üpad','·þÎñÔ±','²ÍÌü','2009-04-20',0,0,2,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
INSERT INTO `employee` VALUES (339,1,'7834','7834','ÎâÏò¶«','³ø·¿','³ø·¿','2080-04-20',100,0,0,'1100000000010100001011000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (340,1,'621','621','Áõ·Æ·Æ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (341,1,'121','121','¹ù¿¼Âú','Áì°à','Ç°Ìü','2020-09-24',0,0,0,'1101011000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (342,1,'622','622','Ö£Ðã','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (343,1,'623','623','ÕÅº£Ó¢','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (344,1,'625','625','Íô¿ËæÃ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (345,1,'626','626','³ÂÃ÷Ó¢','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (346,1,'628','628','ÍõÅàÅà','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (347,1,'629','629','°×´äÇÙ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (348,1,'821','821','ÍõæÃ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (349,1,'822','822','Â¬¸Õ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (350,1,'823','823','Òü×Ð½¡','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (351,1,'825','825','ÅíÊ«Òå','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (352,1,'826','826','ºÎÐãÎÄ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (353,1,'828','828','ÎÂÐ¡ºì','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (354,1,'829','829','ÚÛ·Ò','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (355,1,'921','921','ÕÅ±¾ÏÍ','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (356,1,'922','922','ÍõÓÀºç','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (357,1,'923','923','×ÞÐ¡Àö','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (358,1,'925','925','ÁõÇÉÕä','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (359,1,'926','926','Íõº£Ñà','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (360,1,'928','928','ÌÆÏéÁú','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (361,1,'929','929','ÀîÁúÁú','·þÎñÔ±','Ç°Ìü','2020-09-24',0,0,0,'1100001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (362,1,'122','122','Ð¤ÎÄÐã','Áì°à','Ç°Ìü','2020-09-24',0,0,0,'1101011000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (363,1,'123','123','µËÏþºì','Áì°à','Ç°Ìü','2020-09-24',0,0,0,'1101011000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (364,1,'125','125','³Â½Ü','Áì°à','Ç°Ìü','2020-09-24',0,0,0,'1101011000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',0);
INSERT INTO `employee` VALUES (365,1,'8701','8701','ÈÎÞÈ½àpad','µã²Ë','ÊÕÒø','2080-04-20',99,2,2,'1101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',2);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_info`
--

DROP TABLE IF EXISTS `login_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `login_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(16) NOT NULL,
  `mac_add` varchar(64) NOT NULL,
  `ip_add` varchar(16) NOT NULL,
  `licence_code` varchar(128) NOT NULL,
  `or_id` int(10) unsigned DEFAULT '0',
  `authority` int(10) unsigned DEFAULT '0',
  `action` int(10) unsigned DEFAULT '1',
  `action_info` varchar(64) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mac_add` (`mac_add`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `login_info`
--

LOCK TABLES `login_info` WRITE;
/*!40000 ALTER TABLE `login_info` DISABLE KEYS */;
INSERT INTO `login_info` VALUES (1,'1','00145E5C67BC','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (2,'1','003018A49C56','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (11,'1','8000600FE800','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (16,'1','003018A49B58','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (17,'1','00164497ED99','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (18,'1','0016CB1B9BE4','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (19,'1','74DE2B68B386','127.0.0.1','A1C619E7C845AA89F4ADD35E556920E7',0,1,1,'ÊÕÒøµÇÂ½','2012-06-17 19:31:52');
INSERT INTO `login_info` VALUES (20,'1','B0487A0D4076','127.0.0.1','9AA19EC7F3C68C02E17E280E30AEBA0B',0,1,1,'ÊÕÒøµÇÂ½','2012-09-11 13:35:39');
INSERT INTO `login_info` VALUES (21,'1','6FDF35FF-2','','',0,1,0,'µã²ËÆ÷µÇÂ¼','2012-09-11 11:21:19');
INSERT INTO `login_info` VALUES (22,'600500','80EE733BA307','127.0.0.1','A09BBB7936AC0A333B352C3D854B7BD1',0,1,1,'ÊÕÒøµÇÂ½','2012-09-22 17:37:43');
INSERT INTO `login_info` VALUES (23,'1','bfff9ffbb8','','',0,1,0,'µã²ËÆ÷µÇÂ¼','2012-09-11 18:42:20');
INSERT INTO `login_info` VALUES (25,'600500','E091536FAC7A','127.0.0.1','A9258C6E1FDD0827CC3B37ADC0C68414',0,1,1,'ÊÕÒøµÇÂ½','2012-09-22 17:44:05');
INSERT INTO `login_info` VALUES (26,'600500','0025AB21929E','192.168.0.31','74387117EAF6B0C5D65E5146778C1FD9',0,1,1,'ÊÕÒøµÇÂ½','2012-09-23 11:19:19');
INSERT INTO `login_info` VALUES (27,'000885','4437E6A78E9F','192.168.0.1','43995BF082BD9D926510DE77BD3F1297',0,1,1,'ÊÕÒøµÇÂ½','2012-09-24 13:07:03');
INSERT INTO `login_info` VALUES (29,'8701','0025AB212208','192.168.0.29','C5F8A846924ABE4845EB0F72790F19E0',0,1,1,'ÊÕÒøµÇÂ½','2012-09-24 11:56:19');
INSERT INTO `login_info` VALUES (30,'885','705cc62098','','',0,1,0,'µã²ËÆ÷µÇÂ¼','2012-09-24 13:04:24');
/*!40000 ALTER TABLE `login_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `orders` (
  `or_id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `or_name` varchar(32) NOT NULL,
  `combine_order` int(11) DEFAULT NULL,
  `is_save` int(10) unsigned DEFAULT '1',
  `status` int(10) unsigned NOT NULL,
  `order_type` varchar(16) NOT NULL,
  `people_num` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `preference` double DEFAULT NULL,
  `erase` double DEFAULT '0',
  `discount` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `cover_charge` double DEFAULT NULL,
  `payment` double DEFAULT NULL,
  `pay_taxes` double DEFAULT '0',
  `taxes_num` varchar(64) DEFAULT NULL,
  `taxes_address` varchar(64) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  `waiter` varchar(16) NOT NULL,
  `creator` varchar(16) NOT NULL,
  `finisher` varchar(16) NOT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `fiscal_period` date DEFAULT NULL,
  `current_period` int(11) DEFAULT '1',
  `is_print_pantry` int(10) unsigned DEFAULT '0',
  `pantry_time` time DEFAULT NULL,
  `print_count_1` int(11) DEFAULT '0',
  `print_count_2` int(11) DEFAULT '0',
  `print_over` int(11) DEFAULT '0',
  PRIMARY KEY (`or_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_item`
--

DROP TABLE IF EXISTS `orders_item`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `orders_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `or_name` varchar(32) DEFAULT NULL,
  `is_save` int(10) unsigned DEFAULT '1',
  `status` int(10) unsigned NOT NULL,
  `finished_rush` int(10) unsigned NOT NULL,
  `is_rush` int(10) unsigned NOT NULL,
  `print_error` int(10) unsigned DEFAULT '0',
  `print_error_name` varchar(32) DEFAULT NULL,
  `print_index` int(10) unsigned DEFAULT '0',
  `is_print` int(10) unsigned DEFAULT '0',
  `mix` int(10) unsigned NOT NULL,
  `batch_series` varchar(64) NOT NULL,
  `item_series_no` varchar(16) NOT NULL,
  `item_series_name` varchar(64) NOT NULL,
  `item_series_quantity` int(10) unsigned NOT NULL,
  `item_no` varchar(16) NOT NULL,
  `item_name` varchar(64) NOT NULL,
  `item_price` double DEFAULT NULL,
  `item_unit` varchar(8) NOT NULL,
  `item_selling_unit` varchar(8) NOT NULL,
  `item_selling_quantity` double DEFAULT NULL,
  `item_quantity` double DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_prefer` double DEFAULT NULL,
  `add_in_count` int(10) unsigned NOT NULL,
  `add_in_price` double DEFAULT NULL,
  `add_in` varchar(128) NOT NULL,
  `item_creator` varchar(16) NOT NULL,
  `create_time` time NOT NULL,
  `finished_cancel` double DEFAULT NULL,
  `is_cancel` double DEFAULT NULL,
  `cancel_operator` varchar(16) NOT NULL,
  `is_present` double DEFAULT NULL,
  `present_operator` varchar(16) NOT NULL,
  `is_deliver` int(10) unsigned DEFAULT '0',
  `deliver_time` time DEFAULT NULL,
  `is_print_pantry` int(10) unsigned DEFAULT '0',
  `pantry_time` time DEFAULT NULL,
  `print_time` time DEFAULT NULL,
  `rush_time` time DEFAULT NULL,
  `cancel_time` time DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `batch_series` (`batch_series`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `orders_item`
--

LOCK TABLES `orders_item` WRITE;
/*!40000 ALTER TABLE `orders_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `stat_name` varchar(64) NOT NULL,
  `fraction` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (1,'ÏÖ½ð','',4);
INSERT INTO `payment` VALUES (2,'»áÔ±¿¨','',2);
INSERT INTO `payment` VALUES (3,'ÐÅÓÃ¿¨','',4);
INSERT INTO `payment` VALUES (4,'²Í„»','',4);
INSERT INTO `payment` VALUES (5,'¹ÒÕË','',4);
INSERT INTO `payment` VALUES (7,'ÆäËû','',4);
INSERT INTO `payment` VALUES (8,'ÕÐ´ý','',4);
INSERT INTO `payment` VALUES (9,'´ú½ð¾í','',4);
INSERT INTO `payment` VALUES (10,'¶¨½ð','',4);
INSERT INTO `payment` VALUES (11,'Ãâµ¥','',4);
INSERT INTO `payment` VALUES (12,'Ö§Æ±','',4);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_log`
--

DROP TABLE IF EXISTS `payment_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `payment_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `or_id` int(11) NOT NULL,
  `or_name` varchar(32) DEFAULT NULL,
  `payment_name` varchar(64) NOT NULL,
  `payment_valuta` double NOT NULL,
  `payment_user` varchar(16) NOT NULL,
  `status` int(11) NOT NULL,
  `cancel_user` varchar(16) DEFAULT NULL,
  `payment_time` datetime DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `payment_log`
--

LOCK TABLES `payment_log` WRITE;
/*!40000 ALTER TABLE `payment_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_info`
--

DROP TABLE IF EXISTS `pos_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pos_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac_add` varchar(64) NOT NULL,
  `user_id` varchar(16) NOT NULL,
  `ip_add` varchar(16) NOT NULL,
  `action` int(10) unsigned DEFAULT '1',
  `action_info` varchar(64) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `message` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mac_add` (`mac_add`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pos_info`
--

LOCK TABLES `pos_info` WRITE;
/*!40000 ALTER TABLE `pos_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer`
--

DROP TABLE IF EXISTS `printer`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `postscript` varchar(64) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `port` varchar(8) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `spare_printer` int(10) unsigned NOT NULL,
  `page_weight` int(10) unsigned DEFAULT '80',
  `is_table` int(10) unsigned DEFAULT '0',
  `is_pantry` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `printer`
--

LOCK TABLES `printer` WRITE;
/*!40000 ALTER TABLE `printer` DISABLE KEYS */;
INSERT INTO `printer` VALUES (3,'º£ÏÊ´òÓ¡»ú','º£ÏÊ´òÓ¡»ú','192.168.0.60','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',5,80,1,0);
INSERT INTO `printer` VALUES (4,'Àä²Ë´òÓ¡»ú','Àä²Ë´òÓ¡»ú','192.168.0.64','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',8,80,1,0);
INSERT INTO `printer` VALUES (5,'ÈÈ²Ë´òÓ¡»ú','ÈÈ²Ë´òÓ¡»ú','192.168.0.61','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',7,80,1,0);
INSERT INTO `printer` VALUES (6,'Å©¼Ò²Ë´òÓ¡»ú','Å©¼Ò²Ë´òÓ¡»ú','192.168.0.62','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',7,80,1,0);
INSERT INTO `printer` VALUES (7,'ÕôÁýìÒ×Ð´òÓ¡»ú','ÕôÁýìÒ×Ð´òÓ¡»ú','192.168.0.63','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',6,80,1,0);
INSERT INTO `printer` VALUES (8,'Â±Ë®ÉÕÀ°´òÓ¡»ú','Â±Ë®ÉÕÀ°´òÓ¡»ú','192.168.0.65','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',4,80,1,0);
INSERT INTO `printer` VALUES (9,'¾ÆË®´òÓ¡»ú','¾ÆË®´òÓ¡»ú','192.168.0.66','10001','ÈÈÃô´òÓ¡»ú(Ð¡×ÖÌå)',0,80,1,0);
/*!40000 ALTER TABLE `printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rebate`
--

DROP TABLE IF EXISTS `rebate`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `rebate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `ab` varchar(64) NOT NULL,
  `rebate_percent` double DEFAULT NULL,
  `postscript` varchar(64) NOT NULL,
  `type` varchar(64) NOT NULL,
  `useful_life_begin` date DEFAULT NULL,
  `useful_life_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `rebate`
--

LOCK TABLES `rebate` WRITE;
/*!40000 ALTER TABLE `rebate` DISABLE KEYS */;
INSERT INTO `rebate` VALUES (7,1,'½ð¿¨VIP','',0.88,'½ð¿¨VIP','»áÔ±¿¨´òÕÛ','2009-05-15','2015-01-01');
INSERT INTO `rebate` VALUES (8,1,'Òø¿¨VIP','',0.95,'Òø¿¨VIP','»áÔ±¿¨´òÕÛ','2009-05-15','2015-01-01');
INSERT INTO `rebate` VALUES (9,1,'¿ªÒµÈ«³¡88ÕÛ','',0.88,'¿ªÒµÈ«³¡88ÕÛ','»î¶¯ÊÂ¼þ','2011-09-20','2011-10-08');
/*!40000 ALTER TABLE `rebate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rebate_item`
--

DROP TABLE IF EXISTS `rebate_item`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `rebate_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rebate_id` int(11) NOT NULL,
  `course_unit_id` int(11) NOT NULL,
  `type` varchar(64) NOT NULL,
  `rebate_percent` double DEFAULT NULL,
  `rebate_price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `rebate_item`
--

LOCK TABLES `rebate_item` WRITE;
/*!40000 ALTER TABLE `rebate_item` DISABLE KEYS */;
INSERT INTO `rebate_item` VALUES (139,3,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (140,3,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (141,3,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (142,3,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (143,3,0,'ÏÊÕ¥Ë®¹ûÖ­',0,0);
INSERT INTO `rebate_item` VALUES (144,3,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (145,3,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (146,3,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (147,3,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (148,3,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (149,3,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (150,3,0,'²èË®',0,0);
INSERT INTO `rebate_item` VALUES (151,3,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (152,3,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (153,3,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (154,3,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (155,3,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (310,2,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (311,2,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (312,2,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (313,2,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (314,2,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (315,2,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (316,2,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (317,2,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (318,2,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (319,2,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (320,2,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (378,4,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (379,4,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (380,4,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (381,4,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (382,4,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (383,4,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (384,4,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (385,4,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (386,4,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (387,4,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (388,4,0,'²èË®',0,0);
INSERT INTO `rebate_item` VALUES (389,4,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (390,4,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (391,4,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (392,4,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (393,4,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (394,1,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (395,1,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (396,1,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (397,1,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (398,1,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (399,1,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (400,1,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (401,1,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (402,1,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (403,1,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (404,1,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (420,7,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (421,7,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (422,7,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (423,7,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (424,7,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (425,7,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (426,7,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (427,7,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (428,7,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (429,7,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (430,7,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (431,7,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (432,7,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (433,7,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (434,7,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (435,8,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (436,8,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (437,8,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (438,8,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (439,8,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (440,8,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (441,8,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (442,8,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (443,8,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (444,8,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (445,8,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (446,8,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (447,8,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (448,8,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (449,8,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (450,6,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (451,6,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (452,6,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (453,6,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (454,6,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (455,6,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (456,6,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (457,6,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (458,6,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (459,6,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (460,6,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (461,6,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (462,6,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (463,6,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (464,7,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (465,7,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (466,7,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (467,7,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (468,7,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (469,7,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (470,7,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (471,7,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (472,7,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (473,7,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (474,7,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (475,7,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (476,7,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (477,7,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (478,7,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (479,7,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (480,8,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (481,8,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (482,8,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (483,8,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (484,8,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (485,8,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (486,8,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (487,8,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (488,8,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (489,8,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (490,8,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (491,8,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (492,8,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (493,8,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (494,8,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (495,8,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (496,8,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (497,8,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (498,8,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (499,8,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (500,8,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (501,8,0,'Ì×²Í',0,0);
INSERT INTO `rebate_item` VALUES (502,8,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (503,8,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (504,8,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (505,8,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (506,8,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (507,8,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (508,8,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (509,8,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (510,8,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (511,8,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (512,8,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (513,8,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (514,8,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (515,8,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (516,8,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (517,8,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (518,8,0,'Ì×²Í',0,0);
INSERT INTO `rebate_item` VALUES (519,8,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (520,8,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (521,8,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (522,8,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (523,8,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (524,8,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (525,8,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (526,8,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (527,8,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (528,8,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (529,8,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (530,7,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (531,7,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (532,7,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (533,7,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (534,7,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (535,7,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (536,7,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (537,7,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (538,7,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (539,7,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (540,7,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (541,7,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (542,7,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (543,7,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (544,7,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (545,7,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (546,8,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (547,8,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (548,8,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (549,8,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (550,8,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (551,8,0,'Ì×²Í',0,0);
INSERT INTO `rebate_item` VALUES (552,8,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (553,8,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (554,8,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (555,8,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (556,8,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (557,8,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (558,8,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (559,8,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (560,8,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (561,8,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (562,8,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (563,9,0,'ÁúÏº',0,0);
INSERT INTO `rebate_item` VALUES (564,9,0,'Á¹²Ë',0,0);
INSERT INTO `rebate_item` VALUES (565,9,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (566,9,0,'ìÒ×Ð',0,0);
INSERT INTO `rebate_item` VALUES (567,9,0,'Ìú°åÉÕ',0,0);
INSERT INTO `rebate_item` VALUES (568,9,0,'ÌØÉ«¹¦·òÌÀ',0,0);
INSERT INTO `rebate_item` VALUES (569,9,0,'Íß¹Þ',0,0);
INSERT INTO `rebate_item` VALUES (570,9,0,'ÔÁ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (571,9,0,'Õô²Ë',0,0);
INSERT INTO `rebate_item` VALUES (572,9,0,'±«Óã£¬Óã³á',0,0);
INSERT INTO `rebate_item` VALUES (573,9,0,'´®ÉÕÀà',0,0);
INSERT INTO `rebate_item` VALUES (574,9,0,'µãÐÄ',0,0);
INSERT INTO `rebate_item` VALUES (575,9,0,'·É±ý',0,0);
INSERT INTO `rebate_item` VALUES (576,9,0,'¸É¹øµµ',0,0);
INSERT INTO `rebate_item` VALUES (577,9,0,'º£ÏÊ',0,0);
INSERT INTO `rebate_item` VALUES (578,9,0,'¿¾Ñ¼',0,0);
INSERT INTO `rebate_item` VALUES (579,9,0,'Àä²Ë',0,0);
INSERT INTO `rebate_item` VALUES (580,9,0,'ÈÈ²Ë',0,0);
INSERT INTO `rebate_item` VALUES (581,9,0,'ÉÕÀ°',0,0);
INSERT INTO `rebate_item` VALUES (582,9,0,'ÕôÁýìÒ×Ð',0,0);
/*!40000 ALTER TABLE `rebate_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remark`
--

DROP TABLE IF EXISTS `remark`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `ab` varchar(64) NOT NULL,
  `price` double DEFAULT NULL,
  `is_modif_price` tinyint(1) DEFAULT NULL,
  `type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `number` (`number`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `remark`
--

LOCK TABLES `remark` WRITE;
/*!40000 ALTER TABLE `remark` DISABLE KEYS */;
INSERT INTO `remark` VALUES (1,'1','ºìÉÕ','hs',0,0,'');
INSERT INTO `remark` VALUES (2,'2','°××Æ','bz',0,0,'');
INSERT INTO `remark` VALUES (3,'3','¶ç½·','dj',0,0,'');
INSERT INTO `remark` VALUES (4,'4','ìÒÌÀ','bt',0,0,'');
INSERT INTO `remark` VALUES (5,'5','Ìú°å','tb',10,1,'');
INSERT INTO `remark` VALUES (6,'6','³áÌÀ','ct',10,1,'');
INSERT INTO `remark` VALUES (7,'7','ËâÈ×','sr',0,0,'');
INSERT INTO `remark` VALUES (8,'8','¶ç½·Õô','djz',0,0,'');
INSERT INTO `remark` VALUES (9,'9','Ñ©²ËºìÉÕ','xchs',0,0,'');
INSERT INTO `remark` VALUES (10,'10','½·ÑÎ','jy',0,0,'');
INSERT INTO `remark` VALUES (11,'11','ÇåÕô','qz',0,0,'');
INSERT INTO `remark` VALUES (12,'12','Éú³é','sc',0,0,'');
INSERT INTO `remark` VALUES (13,'13','´óËâ³´','dsc',8,1,'');
INSERT INTO `remark` VALUES (15,'14','ìÀµ°','dd',10,1,'');
INSERT INTO `remark` VALUES (16,'15','½ª´Ð³´','jcc',0,0,'');
INSERT INTO `remark` VALUES (17,'16','¾Â²Ë³´','jcc',8,1,'');
INSERT INTO `remark` VALUES (18,'17','Ãæ½î','mj',0,0,'');
INSERT INTO `remark` VALUES (19,'18','ÄÏÈéÖ­','nlz',0,0,'');
INSERT INTO `remark` VALUES (20,'19','²»·Å´Ð','bfc',0,0,'');
INSERT INTO `remark` VALUES (21,'20','²»·Å½ª','bfj',0,0,'');
INSERT INTO `remark` VALUES (22,'21','²»·ÅÌÇ','bft',0,0,'');
INSERT INTO `remark` VALUES (23,'22','²»·ÅËâ','bfs',0,0,'');
INSERT INTO `remark` VALUES (24,'23','²»·ÅÏã²Ë','bfxc',0,0,'');
INSERT INTO `remark` VALUES (26,'24','Ïã¹½','xg',0,1,'');
INSERT INTO `remark` VALUES (27,'25','Üú°×ÈâË¿','jbrs',15,1,'');
INSERT INTO `remark` VALUES (28,'26','ÓãÏãÇÑ×Ó','yxqz',15,1,'');
INSERT INTO `remark` VALUES (29,'27','Ë¿¹Ï³´µ°','sgcd',12,1,'');
INSERT INTO `remark` VALUES (30,'28','Ë¿¹ÏÃæ½î','sgmj',12,1,'');
INSERT INTO `remark` VALUES (31,'29','Ë¿¹ÏÃ«¶¹','sgmd',12,1,'');
INSERT INTO `remark` VALUES (32,'30','¾Â²ËÈâË¿','jcrs',0,1,'');
INSERT INTO `remark` VALUES (33,'31','¾Â²Ë¼¦µ°','jcjd',12,1,'');
INSERT INTO `remark` VALUES (35,'32','¼Ó¼±ÉÏ','jjs',0,0,'');
INSERT INTO `remark` VALUES (36,'33','±ùÕò','bz',0,0,'');
INSERT INTO `remark` VALUES (37,'34','ÏãÀ±','xl',0,0,'');
INSERT INTO `remark` VALUES (38,'35','ÇåË®','qs',0,0,'');
INSERT INTO `remark` VALUES (39,'36','Ñç»á¶þ×À','yhez',0,0,'');
INSERT INTO `remark` VALUES (40,'37','Ñç»áÈý×À','yhsz',0,0,'');
INSERT INTO `remark` VALUES (41,'38','Ñç»áËÄ×À','yhsz',0,0,'');
/*!40000 ALTER TABLE `remark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `reservation` (
  `resv_id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `or_id` int(11) NOT NULL,
  `or_name` varchar(32) DEFAULT NULL,
  `resv_name` varchar(32) NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `order_type` varchar(16) NOT NULL,
  `reservation_date` datetime DEFAULT NULL,
  `people_num` double DEFAULT NULL,
  `people_price` double DEFAULT NULL,
  `contact_phone` varchar(32) NOT NULL,
  `guest_name` varchar(32) NOT NULL,
  `guest_business` varchar(64) NOT NULL,
  `guest_type` varchar(32) NOT NULL,
  `remark` varchar(32) NOT NULL,
  `receive_name` varchar(32) NOT NULL,
  `receive_phone` varchar(32) NOT NULL,
  `creator` varchar(16) NOT NULL,
  `finisher` varchar(16) NOT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `is_hasten_order` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`resv_id`),
  UNIQUE KEY `resv_name` (`resv_name`)
) ENGINE=InnoDB AUTO_INCREMENT=532 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (1,1,50,0,NULL,'2009042915352850',0,'É¢Ì¨','2009-04-29 15:35:28',14,0,'13861882955','ÖìÎ°(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-04-29 15:54:18','2009-04-29 19:35:28',0);
INSERT INTO `reservation` VALUES (2,1,56,0,NULL,'2009042915372256',1,'É¢Ì¨','2009-04-29 15:37:22',10,0,'15052121301','¶­(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1302','','2009-04-29 17:54:12','2009-04-29 19:37:22',0);
INSERT INTO `reservation` VALUES (3,1,74,0,NULL,'2009042915392874',0,'É¢Ì¨','2009-04-29 15:39:28',50,0,'','ÍõÈýÐË(¾­Àí)','','ÖØÒª¿Í»§','','³ÂÓÀ·¢','','1302','','2009-04-29 15:57:18','2009-04-29 19:39:28',0);
INSERT INTO `reservation` VALUES (4,1,60,0,NULL,'2009042916535560',1,'É¢Ì¨','2009-04-29 16:53:55',9,0,'15061823967','ÉÛ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1302','','2009-04-29 17:53:58','2009-04-29 20:53:55',0);
INSERT INTO `reservation` VALUES (5,1,50,102,'200904301154070500102','2009043010311950',3,'É¢Ì¨','2009-04-30 10:31:19',10,0,'','ÑÕ×Ü(¾­Àí)','Ã÷´ï·ÄÆ÷','ÖØÒª¿Í»§','','','','1302','1102','2009-04-30 11:54:07','2009-04-30 12:02:52',0);
INSERT INTO `reservation` VALUES (6,1,54,0,NULL,'2009043010312754',1,'É¢Ì¨','2009-04-30 10:31:27',3,0,'','ÐìÐÐ³¤(ÏÈÉú)','','ÖØÒª¿Í»§','','ÈýÃ«','','1302','','2009-04-30 11:50:30','2009-04-30 14:31:27',0);
INSERT INTO `reservation` VALUES (7,1,64,0,NULL,'2009043010320864',1,'É¢Ì¨','2009-04-30 10:32:08',10,0,'','»ª×Ü(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-04-30 11:55:00','2009-04-30 12:03:46',0);
INSERT INTO `reservation` VALUES (8,1,65,0,NULL,'2009043010325465',1,'É¢Ì¨','2009-04-30 10:32:54',10,0,'','Â½×Ü¶©(¾­Àí)','','ÖØÒª¿Í»§','','Â½×Ü¶©','','1102','','2009-04-30 11:54:45','2009-04-30 12:03:30',0);
INSERT INTO `reservation` VALUES (9,1,68,0,NULL,'2009043010334968',1,'É¢Ì¨','2009-04-30 10:33:49',12,0,'','ÐÅ·Ã¾Ö(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«','','1302','','2009-04-30 11:33:56','2009-04-30 14:33:49',0);
INSERT INTO `reservation` VALUES (10,1,71,96,'200904301136110710096','2009043010343471',3,'Ñç»á','2009-04-30 10:34:34',30,0,'','ÕÅÓÀ¿ü(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1302','1102','2009-04-30 11:36:11','2009-04-30 11:44:57',0);
INSERT INTO `reservation` VALUES (11,1,74,0,NULL,'2009043010354374',1,'É¢Ì¨','2009-04-30 10:35:43',10,0,'','ÕÅÓÀ¿ü(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-04-30 11:38:23','2009-04-30 11:47:09',0);
INSERT INTO `reservation` VALUES (12,1,50,0,NULL,'2009043015231150',0,'É¢Ì¨','2009-04-30 15:23:11',12,0,'','Îý¸Ö()','','ÖØÒª¿Í»§','','','','1302','','2009-04-30 15:41:32','2009-04-30 19:23:11',0);
INSERT INTO `reservation` VALUES (13,1,52,0,NULL,'2009043015235452',1,'É¢Ì¨','2009-04-30 15:23:54',6,0,'13812287391','ºú(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-04-30 19:24:03','2009-04-30 19:23:54',0);
INSERT INTO `reservation` VALUES (14,1,54,0,NULL,'2009043015244854',1,'É¢Ì¨','2009-04-30 15:24:48',10,0,'','²ÉÃº»ú³§(¾­Àí)','²ÉÃº»ú³§','ÖØÒª¿Í»§','','´ó½ã¶©','','8868','','2009-04-30 19:24:42','2009-04-30 19:24:48',0);
INSERT INTO `reservation` VALUES (15,1,56,0,NULL,'2009043015282856',1,'É¢Ì¨','2009-04-30 15:28:28',10,0,'15961772910','¸ê(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-04-30 19:24:46','2009-04-30 19:28:28',0);
INSERT INTO `reservation` VALUES (16,1,58,0,NULL,'2009043015303558',1,'É¢Ì¨','2009-04-30 15:30:35',10,0,'','°üÊ¦¸µ(ÏÈÉú)','','ÖØÒª¿Í»§','','´ó½ã¶©','','8868','','2009-04-30 19:23:57','2009-04-30 19:30:35',0);
INSERT INTO `reservation` VALUES (17,1,60,0,NULL,'2009043015313660',1,'É¢Ì¨','2009-04-30 15:31:36',10,0,'13093020865','Íõ(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','8868','','2009-04-30 19:24:12','2009-04-30 19:31:36',0);
INSERT INTO `reservation` VALUES (18,1,62,0,NULL,'2009043015322362',1,'É¢Ì¨','2009-04-30 15:32:23',10,0,'','ÑîÀÏÊ¦(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','ÈýÃ«','','8868','','2009-04-30 19:24:20','2009-04-30 19:32:23',0);
INSERT INTO `reservation` VALUES (19,1,64,0,NULL,'2009043015331864',1,'É¢Ì¨','2009-04-30 15:33:18',13,0,'','Áú¸ç(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«','','8868','','2009-04-30 19:24:36','2009-04-30 19:33:18',0);
INSERT INTO `reservation` VALUES (20,1,65,0,NULL,'2009043015341065',1,'É¢Ì¨','2009-04-30 15:34:10',1,0,'','³ÂÁéÆ¼(Ð¡½ã)','','ÖØÒª¿Í»§','','ÈýÃ«','','8868','','2009-04-30 19:24:26','2009-04-30 19:34:10',0);
INSERT INTO `reservation` VALUES (21,1,67,0,NULL,'2009043015345867',1,'É¢Ì¨','2009-04-30 15:34:58',16,0,'13083502420','ÖÜ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','8868','','2009-04-30 19:24:32','2009-04-30 19:34:58',0);
INSERT INTO `reservation` VALUES (22,1,68,0,NULL,'2009043015353468',1,'É¢Ì¨','2009-04-30 15:35:34',1,0,'','ÑïÃû½ÖµÀÖìÊé¼Ç(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«','','1302','','2009-04-30 16:51:59','2009-04-30 19:35:34',0);
INSERT INTO `reservation` VALUES (23,1,1,0,NULL,'200904301537421',1,'Ñç»á','2009-04-30 15:37:42',270,0,'13003374287','ÐìµÂöÎ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-04-30 15:56:55','2009-04-30 19:37:42',0);
INSERT INTO `reservation` VALUES (24,1,1,0,NULL,'200904301539061',0,'»éÑç','2009-04-30 15:39:06',270,0,'13003374287','ÐìµÂöÎ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-04-30 15:57:33','2009-04-30 19:39:06',0);
INSERT INTO `reservation` VALUES (25,1,79,0,NULL,'2009043016384179',1,'É¢Ì¨','2009-04-30 16:38:41',4,0,'','£±£²£³(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-04-30 17:07:23','2009-04-30 20:38:41',0);
INSERT INTO `reservation` VALUES (26,1,48,0,NULL,'2009043017064348',1,'É¢Ì¨','2009-04-30 17:06:43',4,0,'1223','21(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-04-30 17:07:29','2009-04-30 21:06:43',0);
INSERT INTO `reservation` VALUES (27,1,50,0,NULL,'2009050108561850',1,'É¢Ì¨','2009-05-01 08:56:18',12,0,'13506176490','·®(ÏÈÉú)','','ÐÂ¿Í»§','','Â½¾­Àí','','8868','','2009-05-01 11:21:00','2009-05-01 11:32:37',0);
INSERT INTO `reservation` VALUES (28,1,54,0,NULL,'2009050108575854',1,'É¢Ì¨','2009-05-01 08:57:58',10,0,'13706193007','£±£²£³(¾­Àí)','','ÆÕÍ¨¿Í»§','','','','8868','','2009-05-01 09:51:54','2009-05-01 09:51:54',0);
INSERT INTO `reservation` VALUES (29,1,50,0,NULL,'2009050115581750',0,'É¢Ì¨','2009-05-01 15:58:17',12,0,'15961799065','Îâ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-01 16:25:47','2009-05-01 19:58:17',0);
INSERT INTO `reservation` VALUES (30,1,52,0,NULL,'2009050115590852',1,'É¢Ì¨','2009-05-01 15:59:08',5,0,'13665123294','ÜÇ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-01 18:13:53','2009-05-01 19:59:08',0);
INSERT INTO `reservation` VALUES (31,1,54,0,NULL,'2009050116000054',0,'É¢Ì¨','2009-05-01 16:00:00',12,0,'13861865668','ÖÓ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-01 16:27:21','2009-05-01 20:00:00',0);
INSERT INTO `reservation` VALUES (32,1,55,0,NULL,'2009050116005655',1,'É¢Ì¨','2009-05-01 16:00:56',20,0,'13814243456','Áõ(Ð¡½ã)','','ÆÕÍ¨¿Í»§','ºÍ309°üÏáÒ»Æð','','','8868','','2009-05-01 17:45:34','2009-05-01 17:45:34',0);
INSERT INTO `reservation` VALUES (33,1,58,0,NULL,'2009050116050258',1,'É¢Ì¨','2009-05-01 16:05:02',1,0,'','ÈýÃ«¶©(¾­Àí)','','ÆÕÍ¨¿Í»§','','','','1316','','2009-05-01 17:36:12','2009-05-01 17:36:12',0);
INSERT INTO `reservation` VALUES (34,1,60,0,NULL,'2009050116111060',1,'É¢Ì¨','2009-05-01 16:11:10',12,0,'13812298951','Öì(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 18:08:00','2009-05-01 18:07:59',0);
INSERT INTO `reservation` VALUES (35,1,62,0,NULL,'2009050116121462',1,'É¢Ì¨','2009-05-01 16:12:14',13,0,'13601481255','¹Ë(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 17:49:26','2009-05-01 17:49:25',0);
INSERT INTO `reservation` VALUES (36,1,64,0,NULL,'2009050116132764',1,'É¢Ì¨','2009-05-01 16:13:27',15,0,'13506176132','ÕÅ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 17:49:17','2009-05-01 17:49:16',0);
INSERT INTO `reservation` VALUES (37,1,65,0,NULL,'2009050116140065',1,'É¢Ì¨','2009-05-01 16:14:00',15,0,'13951518639','Öî(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 17:49:57','2009-05-01 17:49:56',0);
INSERT INTO `reservation` VALUES (38,1,67,0,NULL,'2009050116143567',1,'É¢Ì¨','2009-05-01 16:14:35',14,0,'13914113724','Áõ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 18:07:13','2009-05-01 18:07:12',0);
INSERT INTO `reservation` VALUES (39,1,68,0,NULL,'2009050116151768',1,'É¢Ì¨','2009-05-01 16:15:17',10,0,'','´ï(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1101','','2009-05-01 17:48:41','2009-05-01 17:48:40',0);
INSERT INTO `reservation` VALUES (40,1,73,0,NULL,'2009050116161773',1,'É¢Ì¨','2009-05-01 16:16:17',20,0,'','Íõ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','ºÍ8333Ò»Æð','Àî¾­Àí¶©','','1101','','2009-05-01 19:43:45','2009-05-01 19:43:43',0);
INSERT INTO `reservation` VALUES (41,1,76,0,NULL,'2009050116190176',1,'É¢Ì¨','2009-05-01 16:19:01',12,0,'13915329173','Öì(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-01 19:07:12','2009-05-01 19:07:11',0);
INSERT INTO `reservation` VALUES (42,1,56,0,NULL,'2009050817035756',1,'É¢Ì¨','2009-05-08 17:03:57',11,0,'13921282820','Ó¡(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1303','','2009-05-08 17:58:55','2009-05-08 21:03:57',0);
INSERT INTO `reservation` VALUES (43,1,50,0,NULL,'2009050209153250',1,'É¢Ì¨','2009-05-02 09:15:32',13,0,'15161505175','¹Ë(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1101','','2009-05-02 10:25:48','2009-05-02 10:25:48',0);
INSERT INTO `reservation` VALUES (44,1,54,0,NULL,'2009050209171754',1,'É¢Ì¨','2009-05-02 09:17:17',10,0,'13771486562','ÐÌ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1102','','2009-05-02 11:30:51','2009-05-02 11:30:50',0);
INSERT INTO `reservation` VALUES (45,1,55,107,'200905021203590550107','2009050209180155',3,'É¢Ì¨','2009-05-02 09:18:01',7,0,'13921519345','¶Å(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1302','1390','2009-05-02 12:03:59','2009-05-02 12:04:00',0);
INSERT INTO `reservation` VALUES (46,1,58,0,NULL,'2009050209184758',1,'É¢Ì¨','2009-05-02 09:18:47',6,0,'13083502425','·ë(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1102','','2009-05-02 11:31:21','2009-05-02 11:31:21',0);
INSERT INTO `reservation` VALUES (47,1,74,97,'200905021130300740097','2009050209194974',3,'É¢Ì¨','2009-05-02 09:19:49',20,0,'13951577293','³Â(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1302','1102','2009-05-02 11:30:30','2009-05-02 11:30:30',0);
INSERT INTO `reservation` VALUES (48,1,66,0,NULL,'2009050209203066',1,'É¢Ì¨','2009-05-02 09:20:30',10,0,'138506189996','·½(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1102','','2009-05-02 10:38:54','2009-05-02 10:38:53',0);
INSERT INTO `reservation` VALUES (49,1,72,95,'200905021129100720095','2009050209225672',3,'É¢Ì¨','2009-05-02 09:22:56',24,0,'13812548825','Áõ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','ºÍ8335Ò»Æð  ¶©½ð200','','','1302','1102','2009-05-02 11:29:10','2009-05-02 11:29:10',0);
INSERT INTO `reservation` VALUES (50,1,1,105,'200905021157590010105','200905021038411',3,'»éÑç','2009-05-02 10:38:41',110,0,'85424801','»Æº£¸ù(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','1102','2009-05-02 11:58:00','2009-05-02 11:57:59',0);
INSERT INTO `reservation` VALUES (51,1,2,115,'200905021251520020115','200905021040012',3,'Ñç»á','2009-05-02 10:40:01',70,0,'15152215830','·ëÊ¿Ãñ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','8868','2009-05-02 12:51:52','2009-05-02 12:51:52',0);
INSERT INTO `reservation` VALUES (52,1,3,0,NULL,'200905021040493',1,'É¢Ì¨','2009-05-02 10:40:49',660,0,'13812513264','°¢Èý(ÏÈÉú)','','ÆÕÍ¨¿Í»§','´óÀÏ°å¶©','','','1102','','2009-05-02 12:55:10','2009-05-02 12:55:09',0);
INSERT INTO `reservation` VALUES (53,1,47,104,'200905021148270470104','2009050210421947',3,'É¢Ì¨','2009-05-02 10:42:19',50,0,'15861516010','Ç®½£Çå(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','1102','2009-05-02 11:48:27','2009-05-02 11:48:27',0);
INSERT INTO `reservation` VALUES (54,1,50,0,NULL,'2009050212292950',1,'É¢Ì¨','2009-05-02 12:29:29',12,0,'13921398824','Âí(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1102','','2009-05-02 13:05:03','2009-05-02 13:05:03',0);
INSERT INTO `reservation` VALUES (55,1,52,0,NULL,'2009050215090452',1,'É¢Ì¨','2009-05-02 15:09:04',6,0,'84856277','Åí(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-02 13:57:33','2009-05-02 19:09:04',0);
INSERT INTO `reservation` VALUES (56,1,54,0,NULL,'2009050217094354',1,'É¢Ì¨','2009-05-02 17:09:43',10,0,'13812015282','¿×(Ð¡½ã)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-02 13:57:20','2009-05-02 21:09:43',0);
INSERT INTO `reservation` VALUES (57,1,55,0,NULL,'2009050215101755',1,'É¢Ì¨','2009-05-02 15:10:17',8,0,'13701513085','ÖÜÎÀÐÂ(ÏÈÉú)','','ÖØÒª¿Í»§','','ÈýÃ«¶©','','1302','','2009-05-02 13:57:27','2009-05-02 19:10:17',0);
INSERT INTO `reservation` VALUES (58,1,56,0,NULL,'2009050216110756',1,'É¢Ì¨','2009-05-02 16:11:07',8,0,'','£±(¾­Àí)','','ÆÕÍ¨¿Í»§','','Ð¡ÀÏ°å¶©','','1302','','2009-05-02 13:58:34','2009-05-02 20:11:07',0);
INSERT INTO `reservation` VALUES (59,1,62,0,NULL,'2009050213115162',1,'É¢Ì¨','2009-05-02 13:11:51',8,0,'','ÍõÖÙÁ¼(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-02 13:36:25','2009-05-02 13:36:25',0);
INSERT INTO `reservation` VALUES (60,1,64,0,NULL,'2009050213123564',1,'É¢Ì¨','2009-05-02 13:12:35',12,0,'','»ªÁªÉÛ×Ü(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-05-02 14:23:08','2009-05-02 14:23:08',0);
INSERT INTO `reservation` VALUES (61,1,66,0,NULL,'2009050213132066',1,'É¢Ì¨','2009-05-02 13:13:20',16,0,'','Àî×Ü(¾­Àí)','','ÖØÒª¿Í»§','','ËïÓîºì','','1102','','2009-05-02 13:17:33','2009-05-02 13:17:33',0);
INSERT INTO `reservation` VALUES (62,1,67,0,NULL,'2009050213140667',1,'É¢Ì¨','2009-05-02 13:14:06',12,0,'','Ê©ºÆÁ¼(¾­Àí)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1102','','2009-05-02 13:22:58','2009-05-02 13:22:58',0);
INSERT INTO `reservation` VALUES (63,1,68,0,NULL,'2009050213144868',0,'É¢Ì¨','2009-05-02 13:14:48',10,0,'','ÕÅ(¾­Àí)','Áª·¢µç»ú','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1302','','2009-05-02 13:15:19','2009-05-02 17:14:48',0);
INSERT INTO `reservation` VALUES (64,1,72,0,NULL,'2009050213155172',0,'É¢Ì¨','2009-05-02 13:15:51',20,0,'13093019786','ÖìÈªÈÙ(ÏÈÉú)','','ÆÕÍ¨¿Í»§','ÇÇÇ¨','','','1302','','2009-05-02 13:16:23','2009-05-02 17:15:51',0);
INSERT INTO `reservation` VALUES (65,1,75,0,NULL,'2009050213165375',1,'É¢Ì¨','2009-05-02 13:16:53',20,0,'13812067080','Öì(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1102','','2009-05-02 13:34:20','2009-05-02 13:34:20',0);
INSERT INTO `reservation` VALUES (66,1,71,0,NULL,'2009050213181171',0,'É¢Ì¨','2009-05-02 13:18:11',30,0,'13814272145','Ñî(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-02 13:18:15','2009-05-02 17:18:11',0);
INSERT INTO `reservation` VALUES (67,1,74,0,NULL,'2009050213184374',0,'É¢Ì¨','2009-05-02 13:18:43',20,0,'13861857617','³Â¾ü(ÏÈÉú)','','ÆÕÍ¨¿Í»§','','','','1302','','2009-05-02 13:20:32','2009-05-02 17:18:43',0);
INSERT INTO `reservation` VALUES (68,1,79,0,NULL,'2009050213210479',1,'É¢Ì¨','2009-05-02 13:21:04',1,0,'','´óÀÏ°å¶©(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-02 13:36:04','2009-05-02 13:36:04',0);
INSERT INTO `reservation` VALUES (69,1,58,0,NULL,'2009050213214158',1,'É¢Ì¨','2009-05-02 13:21:41',20,0,'13806175536','¸êÓñÂ×(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ8312Ò»Æð','','','1302','','2009-05-02 13:58:39','2009-05-02 17:21:41',0);
INSERT INTO `reservation` VALUES (70,1,73,0,NULL,'2009050312355673',1,'É¢Ì¨','2009-05-03 12:35:56',10,0,'13921188500','ÖÜÈº(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-03 12:49:46','2009-05-03 16:35:56',0);
INSERT INTO `reservation` VALUES (71,1,49,0,NULL,'2009050317000049',1,'É¢Ì¨','2009-05-03 17:00:00',4,0,'','123(¾­Àí)','','ÖØÒª¿Í»§','','','','1101','','2009-05-03 20:23:37','2009-05-03 20:23:40',0);
INSERT INTO `reservation` VALUES (72,1,73,0,NULL,'2009050317000073',1,'É¢Ì¨','2009-05-03 17:00:00',10,0,'','ÖÜÈº(¾­Àí)','','ÖØÒª¿Í»§','','','','1101','','2009-05-03 19:09:25','2009-05-03 19:09:28',0);
INSERT INTO `reservation` VALUES (73,1,52,0,NULL,'2009050317000052',1,'É¢Ì¨','2009-05-03 17:00:00',5,0,'','Ëï(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1101','','2009-05-03 19:00:42','2009-05-03 19:00:45',0);
INSERT INTO `reservation` VALUES (74,1,58,0,NULL,'2009050411000058',1,'É¢Ì¨','2009-05-04 11:00:00',10,0,'13382202070','Ç®(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1101','','2009-05-04 11:48:21','2009-05-04 11:48:24',0);
INSERT INTO `reservation` VALUES (75,1,60,0,NULL,'2009050411000060',1,'É¢Ì¨','2009-05-04 11:00:00',10,0,'£±£³£¸£±£²£µ£±£¸£³£±£·','Åí(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1101','','2009-05-04 11:45:37','2009-05-04 11:45:40',0);
INSERT INTO `reservation` VALUES (76,1,56,65,'200905041149010560065','2009050411000056',3,'É¢Ì¨','2009-05-04 11:00:00',10,0,'','ÊÙ»á¼Æ¶¨(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-04 11:49:01','2009-05-04 11:49:01',0);
INSERT INTO `reservation` VALUES (77,1,50,0,NULL,'2009050417000050',1,'É¢Ì¨','2009-05-04 17:00:00',4,0,'','123(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-04 13:55:13','2009-05-04 21:00:00',0);
INSERT INTO `reservation` VALUES (78,1,54,0,NULL,'2009050417000054',1,'É¢Ì¨','2009-05-04 17:00:00',10,0,'','Áè(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-04 18:14:28','2009-05-04 18:14:32',0);
INSERT INTO `reservation` VALUES (79,1,56,0,NULL,'2009050417000056',1,'É¢Ì¨','2009-05-04 17:00:00',6,0,'£±£³£¹£µ£±£µ£°£µ£¸£µ£µ','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-04 18:31:21','2009-05-04 21:00:00',0);
INSERT INTO `reservation` VALUES (80,1,62,0,NULL,'2009050417000062',1,'É¢Ì¨','2009-05-04 17:00:00',9,0,'','Â½×Ü¶¨(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-04 17:52:38','2009-05-04 21:00:00',0);
INSERT INTO `reservation` VALUES (81,1,58,0,NULL,'2009050417000058',1,'É¢Ì¨','2009-05-04 17:00:00',5,0,'£±£³£¹£¶£±£¸£´£¸£µ£·£¹','Àî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-04 18:36:51','2009-05-04 18:36:55',0);
INSERT INTO `reservation` VALUES (82,1,68,0,NULL,'2009050417000068',1,'É¢Ì¨','2009-05-04 17:00:00',8,0,'','ÀÏ°å¶¨(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-04 18:13:50','2009-05-04 18:13:53',0);
INSERT INTO `reservation` VALUES (87,1,77,0,NULL,'2009050417000077',1,'É¢Ì¨','2009-05-04 17:00:00',4,0,'','½â·Å(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','','2009-05-04 17:58:00','2009-05-04 21:00:00',0);
INSERT INTO `reservation` VALUES (88,1,64,0,NULL,'2009050511000064',1,'É¢Ì¨','2009-05-05 11:00:00',10,0,'','½¯×Ü¶¨(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','','2009-05-05 11:00:12','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (89,1,52,4,'200905051056030520004','2009050511000052',3,'É¢Ì¨','2009-05-05 11:00:00',4,0,'','ÀÏÐí(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-05 10:56:03','2009-05-05 10:56:07',0);
INSERT INTO `reservation` VALUES (90,1,54,0,NULL,'2009050511000054',1,'É¢Ì¨','2009-05-05 11:00:00',10,0,'','Ã÷´ïÔ¶Æ÷(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1101','','2009-05-05 12:40:40','2009-05-05 12:40:44',0);
INSERT INTO `reservation` VALUES (91,1,72,0,NULL,'2009050511000072',0,'Ñç»á','2009-05-05 11:00:00',22,0,'','¸¶×æÔË(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ£¸£³£³£µÊÇÒ»Æð','','','1303','','2009-05-05 17:48:15','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (92,1,73,0,NULL,'2009050511000073',0,'Ñç»á','2009-05-05 11:00:00',4,0,'','¸¶×æÔË(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ£¸£³£³£³ÊÇÒ»Æð','','','1303','','2009-05-05 16:19:14','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (98,1,69,0,NULL,'2009050511000069',0,'É¢Ì¨','2009-05-05 11:00:00',6,0,'','ÀÏ°åÄï¶¨(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-05 17:29:07','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (99,1,71,0,NULL,'2009050511000071',1,'É¢Ì¨','2009-05-05 11:00:00',10,0,'','ÄÏÈª(Ð¡½ã)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','','2009-05-05 17:37:46','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (100,1,58,0,NULL,'2009050511000058',0,'É¢Ì¨','2009-05-05 11:00:00',8,0,'','¹ËÊé¼Ç(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','','2009-05-05 17:34:19','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (101,1,68,0,NULL,'2009050511000068',0,'É¢Ì¨','2009-05-05 11:00:00',10,0,'','ÄÏÈª(Ð¡½ã)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','','2009-05-05 17:38:26','2009-05-05 15:00:00',0);
INSERT INTO `reservation` VALUES (102,1,60,6,'200905061120320600006','2009050611000060',3,'É¢Ì¨','2009-05-06 11:00:00',8,0,'','Ç®Ö÷ÈÎ(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1101','2009-05-06 11:20:32','2009-05-06 11:20:36',0);
INSERT INTO `reservation` VALUES (103,1,66,0,NULL,'2009050611000066',1,'É¢Ì¨','2009-05-06 11:00:00',8,0,'','×£(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1101','','2009-05-06 11:13:33','2009-05-06 11:13:39',0);
INSERT INTO `reservation` VALUES (104,1,54,0,NULL,'2009050611000054',1,'É¢Ì¨','2009-05-06 11:00:00',5,0,'','¿×(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1101','','2009-05-06 11:12:10','2009-05-06 11:12:16',0);
INSERT INTO `reservation` VALUES (105,1,56,0,NULL,'2009050611000056',1,'É¢Ì¨','2009-05-06 11:00:00',10,0,'','µ³Õþ°ì(¾­Àí)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1101','','2009-05-06 15:46:40','2009-05-06 15:46:45',0);
INSERT INTO `reservation` VALUES (106,1,64,0,NULL,'2009050611000064',1,'É¢Ì¨','2009-05-06 11:00:00',12,0,'','½¯×Ü¶¨(¾­Àí)','Áª·¢µçÆ÷','ÖØÒª¿Í»§','','','','1101','','2009-05-06 11:22:07','2009-05-06 11:22:12',0);
INSERT INTO `reservation` VALUES (107,1,68,0,NULL,'2009050611000068',1,'É¢Ì¨','2009-05-06 11:00:00',10,0,'','½¯×Ü¶¨(¾­Àí)','»úÐµ³§','ÖØÒª¿Í»§','','','','1101','','2009-05-06 11:20:08','2009-05-06 11:20:13',0);
INSERT INTO `reservation` VALUES (108,1,67,0,NULL,'2009050611000067',1,'É¢Ì¨','2009-05-06 11:00:00',14,0,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','ÐÂÁª´ó¶Ó','','','1101','','2009-05-06 12:30:34','2009-05-06 12:30:39',0);
INSERT INTO `reservation` VALUES (109,1,62,0,NULL,'2009050611000062',1,'É¢Ì¨','2009-05-06 11:00:00',4,0,'','½¯×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','','','','1101','','2009-05-06 11:39:55','2009-05-06 11:40:00',0);
INSERT INTO `reservation` VALUES (110,1,58,0,NULL,'2009050611000058',1,'É¢Ì¨','2009-05-06 11:00:00',6,0,'','Íõ×Ü(ÏÈÉú)','','ÖØÒª¿Í»§','¸Û¿Ú»úÐµ','ÀÏ°åÄï¶¨','','1101','','2009-05-06 12:30:21','2009-05-06 12:30:26',0);
INSERT INTO `reservation` VALUES (111,1,69,0,NULL,'2009050611000069',1,'É¢Ì¨','2009-05-06 11:00:00',4,0,'','½¯×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','Âí×Ô´ïÁùºÍ£¸£³£±£¶ÊÇÒ»ÆðµÄ','','','1101','','2009-05-06 12:29:03','2009-05-06 12:29:08',0);
INSERT INTO `reservation` VALUES (118,1,55,0,NULL,'2009050611000055',1,'É¢Ì¨','2009-05-06 11:00:00',4,0,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','É¢¿ÍÀÏ°åÄï×Ô¼º°²ÅÅ','','','1101','','2009-05-06 12:30:43','2009-05-06 12:30:48',0);
INSERT INTO `reservation` VALUES (119,1,52,0,NULL,'2009050611000052',1,'É¢Ì¨','2009-05-06 11:00:00',4,0,'','½¯×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','»ªÁª','','','1101','','2009-05-06 13:19:18','2009-05-06 13:19:23',0);
INSERT INTO `reservation` VALUES (120,1,63,0,NULL,'2009050611000063',1,'É¢Ì¨','2009-05-06 11:00:00',10,0,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','¶«º£','','','1101','','2009-05-06 13:21:41','2009-05-06 13:21:46',0);
INSERT INTO `reservation` VALUES (122,1,55,0,NULL,'2009050617000055',1,'É¢Ì¨','2009-05-06 17:00:00',6,0,'13906185752','Ð»(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-06 18:26:31','2009-05-06 21:00:00',0);
INSERT INTO `reservation` VALUES (123,1,56,40,'200905061814170560040','2009050617000056',3,'É¢Ì¨','2009-05-06 17:00:00',10,0,'','¶¡(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-06 18:14:17','2009-05-06 18:14:17',0);
INSERT INTO `reservation` VALUES (124,1,58,41,'200905061814320580041','2009050617000058',3,'É¢Ì¨','2009-05-06 17:00:00',10,0,'','³Â(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-06 18:14:32','2009-05-06 18:14:32',0);
INSERT INTO `reservation` VALUES (125,1,68,0,NULL,'2009050617000068',1,'É¢Ì¨','2009-05-06 17:00:00',8,0,'','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','´óÀÏ°å¶¨Õ½ÓÑ','','','1101','','2009-05-06 18:16:18','2009-05-06 18:16:23',0);
INSERT INTO `reservation` VALUES (126,1,53,33,'200905061749030530033','2009050617000053',3,'É¢Ì¨','2009-05-06 17:00:00',7,0,'£±£³£¸£±£²£°£¶£µ£´£±£±','·ë(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-06 17:49:03','2009-05-06 17:49:03',0);
INSERT INTO `reservation` VALUES (127,1,62,29,'200905061735560620029','2009050617000062',3,'É¢Ì¨','2009-05-06 17:00:00',10,0,'','ÍõÓñÇÚ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-06 17:35:56','2009-05-06 17:35:56',0);
INSERT INTO `reservation` VALUES (128,1,52,45,'200905061859410520045','2009050617000052',3,'É¢Ì¨','2009-05-06 17:00:00',6,0,'','Àî¾­Àí¶¨(¾­Àí)','','ÖØÒª¿Í»§','Àî¾­Àí×Ô¼º°²ÅÅ','','','1303','8868','2009-05-06 18:59:41','2009-05-06 18:59:41',0);
INSERT INTO `reservation` VALUES (129,1,59,0,NULL,'2009050617000059',1,'É¢Ì¨','2009-05-06 17:00:00',10,0,'£±£³£¹£¶£±£¸£¸£µ£°£²£·','Ðí(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-06 17:16:37','2009-05-06 21:00:00',0);
INSERT INTO `reservation` VALUES (130,1,70,0,NULL,'2009050611000070',0,'É¢Ì¨','2009-05-06 11:00:00',10,0,'13961885027','Ðí(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-06 17:17:50','2009-05-06 15:00:00',0);
INSERT INTO `reservation` VALUES (141,1,54,34,'200905061749110540034','2009050617000054',3,'É¢Ì¨','2009-05-06 17:00:00',6,0,'','¸Û¿Ú»úÐµ Íõ×Ü(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-06 17:49:11','2009-05-06 17:49:11',0);
INSERT INTO `reservation` VALUES (142,1,63,43,'200905061835480630043','2009050617000063',3,'É¢Ì¨','2009-05-06 17:00:00',10,0,'£±£³£·£°£¶£±£¹£¶£³£±£¶','½¯×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','ÏÖÔÚ»¹²»È·¶¨Ë­','','','1303','8868','2009-05-06 18:35:48','2009-05-06 18:35:48',0);
INSERT INTO `reservation` VALUES (143,1,58,49,'200905071110250580049','2009050711000058',3,'É¢Ì¨','2009-05-07 11:00:00',8,0,'13328110686','ÕÅ(ÏÈÉú)','½­ÄÏÊµÑéÐ¡Ñ§','ÖØÒª¿Í»§','','','','1303','1352','2009-05-07 11:10:25','2009-05-07 11:10:47',0);
INSERT INTO `reservation` VALUES (144,1,64,52,'200905071123000640052','2009050711000064',3,'É¢Ì¨','2009-05-07 11:00:00',10,0,'','Ëï½ðÃ÷(ÏÈÉú)','','ÖØÒª¿Í»§','ÀÏ°åÄïÇë¿Í','','','1303','8868','2009-05-07 11:23:00','2009-05-07 11:23:00',0);
INSERT INTO `reservation` VALUES (145,1,66,0,NULL,'2009050711000066',1,'É¢Ì¨','2009-05-07 11:00:00',10,0,'','ÐíÖÙÁ¼(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1101','','2009-05-07 12:30:35','2009-05-07 12:30:42',0);
INSERT INTO `reservation` VALUES (146,1,50,0,NULL,'2009050717000050',1,'É¢Ì¨','2009-05-07 17:00:00',10,0,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','','2009-05-07 16:27:33','2009-05-07 21:00:00',0);
INSERT INTO `reservation` VALUES (147,1,56,74,'200905071808470560074','2009050717000056',3,'É¢Ì¨','2009-05-07 17:00:00',10,0,'£±£³£¹£¶£±£¸£¸£¸£²£°£²','½­(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','1007','2009-05-07 18:08:47','2009-05-07 18:09:18',0);
INSERT INTO `reservation` VALUES (148,1,64,67,'200905071725110640067','2009050717000064',3,'É¢Ì¨','2009-05-07 17:00:00',4,0,'','ºúÏ²Ã÷(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1308','2009-05-07 17:25:11','2009-05-07 17:25:20',0);
INSERT INTO `reservation` VALUES (149,1,66,65,'200905071709040660065','2009050717000066',3,'É¢Ì¨','2009-05-07 17:00:00',15,0,'','ÖìÐË¶È(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1309','2009-05-07 17:09:04','2009-05-07 17:09:14',0);
INSERT INTO `reservation` VALUES (150,1,67,0,NULL,'2009050717000067',1,'É¢Ì¨','2009-05-07 17:00:00',9,0,'','¹ËÊé¼Ç(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1101','','2009-05-07 18:27:25','2009-05-07 18:27:32',0);
INSERT INTO `reservation` VALUES (151,1,68,63,'200905071650340680063','2009050717000068',3,'É¢Ì¨','2009-05-07 17:00:00',12,0,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','Ì¨°ìµÄ','','','1303','1312','2009-05-07 16:50:34','2009-05-07 16:50:43',0);
INSERT INTO `reservation` VALUES (152,1,71,0,NULL,'2009050717000071',1,'É¢Ì¨','2009-05-07 17:00:00',50,0,'','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','£´±¸£±ºÍ£¸£³£³£¶ÊÇÒ»ÆðµÄ','','','1101','','2009-05-07 18:05:48','2009-05-07 18:05:55',0);
INSERT INTO `reservation` VALUES (153,1,74,0,'200905071804400740073','2009050717000074',1,'É¢Ì¨','2009-05-07 17:00:00',50,0,'','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','ºÍ£¸£³£³£²ÊÇÒ»ÆðµÄ','','','1101','','2009-05-07 18:05:09','2009-05-07 18:05:16',0);
INSERT INTO `reservation` VALUES (154,1,58,0,NULL,'2009050717000058',1,'É¢Ì¨','2009-05-07 17:00:00',6,0,'','¶É(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1101','','2009-05-07 18:29:06','2009-05-07 18:29:13',0);
INSERT INTO `reservation` VALUES (164,1,50,0,NULL,'2009050711000050',0,'É¢Ì¨','2009-05-07 11:00:00',12,0,'13812271021','ÀÏ°åÄï(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','','2009-05-07 17:15:04','2009-05-07 15:00:00',0);
INSERT INTO `reservation` VALUES (165,1,74,0,NULL,'2009050711000074',0,'É¢Ì¨','2009-05-07 11:00:00',50,0,'','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','ºÍ£¸£³£³£²ÊÇÒ»ÆðµÄ','','','1303','','2009-05-07 18:40:30','2009-05-07 15:00:00',0);
INSERT INTO `reservation` VALUES (166,1,62,0,NULL,'2009050811000062',1,'É¢Ì¨','2009-05-08 11:00:00',10,0,'£±£³£²£µ£µ£±£±£°£·£°£·','¹ý(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-08 12:15:07','2009-05-08 15:00:00',0);
INSERT INTO `reservation` VALUES (167,1,65,3,'200905081112360650003','2009050811000065',3,'É¢Ì¨','2009-05-08 11:00:00',10,0,'','ÀÏ°åÄï¶¨(¾­Àí)','ÎÛË®³§','ÖØÒª¿Í»§','','','','1303','1317','2009-05-08 11:12:36','2009-05-08 11:12:47',0);
INSERT INTO `reservation` VALUES (168,1,50,33,'200905081805420500033','2009050817000050',3,'É¢Ì¨','2009-05-08 17:00:00',13,0,'13921282820','Ó¥(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1304','2009-05-08 18:05:42','2009-05-08 18:06:16',0);
INSERT INTO `reservation` VALUES (169,1,63,0,NULL,'2009050817000063',1,'É¢Ì¨','2009-05-08 17:00:00',12,0,'£±£³£¸£±£²£²£¹£¶£¹£µ£¸','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-08 17:35:50','2009-05-08 21:00:00',0);
INSERT INTO `reservation` VALUES (170,1,64,19,'200905081719260640019','2009050817000064',3,'É¢Ì¨','2009-05-08 17:00:00',16,0,'£±£³£¹£¶£±£¸£³£³£·£°£³','³É(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1308','2009-05-08 17:19:26','2009-05-08 17:19:36',0);
INSERT INTO `reservation` VALUES (171,1,65,20,'200905081719550650020','2009050817000065',3,'É¢Ì¨','2009-05-08 17:00:00',10,0,'','°¢ÎåÍ·(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1317','2009-05-08 17:19:55','2009-05-08 17:20:05',0);
INSERT INTO `reservation` VALUES (172,1,66,16,'200905081644470660016','2009050817000066',3,'É¢Ì¨','2009-05-08 17:00:00',4,0,'','ÀÏ°åÄï¶¨(¾­Àí)','ÐÂ¹â´åÕÔÊé¼Ç','ÖØÒª¿Í»§','','','','1303','1309','2009-05-08 16:44:47','2009-05-08 16:44:57',0);
INSERT INTO `reservation` VALUES (173,1,67,18,'200905081717300670018','2009050817000067',3,'É¢Ì¨','2009-05-08 17:00:00',15,0,'£±£³£´£°£°£°£³£²£²£°£µ','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1328','2009-05-08 17:17:30','2009-05-08 17:17:40',0);
INSERT INTO `reservation` VALUES (174,1,68,0,NULL,'2009050817000068',1,'É¢Ì¨','2009-05-08 17:00:00',15,0,'','¹ËÊé¼Ç(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1101','','2009-05-08 18:19:10','2009-05-08 18:19:19',0);
INSERT INTO `reservation` VALUES (175,1,72,24,'200905081742350720024','2009050817000072',3,'É¢Ì¨','2009-05-08 17:00:00',20,0,'13861808216','å§(ÏÈÉú)','','ÖØÒª¿Í»§','ÇÇÇ¨ºÍ£¸£³£³£µÊÇÒ»ÆðµÄ','','','1303','1304','2009-05-08 17:42:35','2009-05-08 17:42:45',0);
INSERT INTO `reservation` VALUES (176,1,75,25,'200905081745380750025','2009050817000075',3,'É¢Ì¨','2009-05-08 17:00:00',20,0,'','ÑÏµ¤µ¤(¾­Àí)','','ÖØÒª¿Í»§','½á»éºÍ8339ÊÇÒ»ÆðµÄ','','','1303','1304','2009-05-08 17:45:38','2009-05-08 17:45:48',0);
INSERT INTO `reservation` VALUES (177,1,74,26,'200905081745460740026','2009050817000074',3,'É¢Ì¨','2009-05-08 17:00:00',60,0,'','Ðì±ø(ÏÈÉú)','Îå×À·Å8332£¬£¬8336Ò»×À·Å8331','ÖØÒª¿Í»§','½á»éºÍ8332¡£8331ÊÇÒ»ÆðµÄ','','','1303','1304','2009-05-08 17:45:46','2009-05-08 17:45:56',0);
INSERT INTO `reservation` VALUES (178,1,79,30,'200905081755530790030','2009050817000079',3,'É¢Ì¨','2009-05-08 17:00:00',21,250,'','ÀÏ°åÄï¶¨(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','1006','2009-05-08 17:55:53','2009-05-08 17:56:27',0);
INSERT INTO `reservation` VALUES (179,1,54,39,'200905081839430540039','2009050817000054',3,'É¢Ì¨','2009-05-08 17:00:00',7,0,'£±£³£·£°£¶£±£¹£´£°£°£¹','ÁÎ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1349','2009-05-08 18:39:43','2009-05-08 18:40:17',0);
INSERT INTO `reservation` VALUES (181,1,52,0,NULL,'2009050817000052',1,'É¢Ì¨','2009-05-08 17:00:00',4,0,'','Íõ×Ü(ÏÈÉú)','´óÁ¦ÆðÖØ','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1101','','2009-05-08 18:10:00','2009-05-08 18:10:08',0);
INSERT INTO `reservation` VALUES (182,1,54,49,'200905091101510540049','2009050911000054',3,'É¢Ì¨','2009-05-09 11:00:00',10,0,'13328101292','Ì¶(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-09 11:01:51','2009-05-09 11:02:26',0);
INSERT INTO `reservation` VALUES (183,1,64,50,'200905091103410640050','2009050911000064',3,'É¢Ì¨','2009-05-09 11:00:00',10,0,'','½¯×Ü¶¨(¾­Àí)','´óÁ¦ÆðÖØ','ÖØÒª¿Í»§','','','','1303','1308','2009-05-09 11:03:42','2009-05-09 11:03:52',0);
INSERT INTO `reservation` VALUES (184,1,68,0,NULL,'2009050911000068',1,'É¢Ì¨','2009-05-09 11:00:00',12,0,'','¼Ö(ÏÈÉú)','','ÖØÒª¿Í»§','','Àî¾­Àí¶¨','','1303','','2009-05-09 10:48:58','2009-05-09 15:00:00',0);
INSERT INTO `reservation` VALUES (185,1,66,55,'200905091109060660055','2009050911000066',3,'É¢Ì¨','2009-05-09 11:00:00',10,0,'','ºã·á(¾­Àí)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1309','2009-05-09 11:09:06','2009-05-09 11:09:17',0);
INSERT INTO `reservation` VALUES (186,1,58,63,'200905091136360580063','2009050911000058',3,'É¢Ì¨','2009-05-09 11:00:00',5,0,'','½¯×Ü¶¨(¾­Àí)','ÖþÂ·»úÐµ³§','ÖØÒª¿Í»§','','','','1303','1344','2009-05-09 11:36:36','2009-05-09 11:37:02',0);
INSERT INTO `reservation` VALUES (187,1,50,0,NULL,'2009050917000050',1,'É¢Ì¨','2009-05-09 17:00:00',15,0,'13961751230','¹Ë(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-09 19:01:19','2009-05-09 19:01:29',0);
INSERT INTO `reservation` VALUES (188,1,52,82,'200905091735290520082','2009050917000052',3,'É¢Ì¨','2009-05-09 17:00:00',5,0,'','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','ÊÙ»á¼Æ¶¨','','1303','1306','2009-05-09 17:35:29','2009-05-09 17:36:05',0);
INSERT INTO `reservation` VALUES (189,1,56,0,NULL,'2009050917000056',1,'É¢Ì¨','2009-05-09 17:00:00',6,0,'','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1303','','2009-05-09 19:49:29','2009-05-09 21:00:00',0);
INSERT INTO `reservation` VALUES (190,1,58,0,NULL,'2009050917000058',1,'É¢Ì¨','2009-05-09 17:00:00',7,0,'£±£µ£¸£¶£±£µ£¹£³£¸£³£¶','Àî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-09 17:00:51','2009-05-09 21:00:00',0);
INSERT INTO `reservation` VALUES (191,1,64,80,'200905091721180640080','2009050917000064',3,'É¢Ì¨','2009-05-09 17:00:00',16,0,'£±£³£¹£¶£±£¸£³£³£·£°£³','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1308','2009-05-09 17:21:18','2009-05-09 17:21:31',0);
INSERT INTO `reservation` VALUES (192,1,66,84,'200905091739150660084','2009050917000066',3,'É¢Ì¨','2009-05-09 17:00:00',16,0,'','ÕÅÐË±¦(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','8868','2009-05-09 17:39:15','2009-05-09 17:39:15',0);
INSERT INTO `reservation` VALUES (193,1,67,85,'200905091740350670085','2009050917000067',3,'É¢Ì¨','2009-05-09 17:00:00',15,0,'£±£³£¹£²£±£±£µ£°£·£¶£²','³ý(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','1367','2009-05-09 17:40:35','2009-05-09 17:40:47',0);
INSERT INTO `reservation` VALUES (194,1,72,86,'200905091740430720086','2009050917000072',3,'É¢Ì¨','2009-05-09 17:00:00',20,0,'','³Ð×Ü(ÏÈÉú)','','ÖØÒª¿Í»§','ºã·áºÍ£¸£³£³£µÊÇÒ»ÆðµÄ','ÀÏ°åÄï¶¨','','1303','1367','2009-05-09 17:40:43','2009-05-09 17:40:55',0);
INSERT INTO `reservation` VALUES (195,1,68,0,NULL,'2009050917000068',1,'É¢Ì¨','2009-05-09 17:00:00',12,0,'','¼Ö(ÏÈÉú)','','ÖØÒª¿Í»§','','Àî¾­Àí¶¨','','1303','','2009-05-09 18:43:39','2009-05-09 21:00:00',0);
INSERT INTO `reservation` VALUES (196,1,71,87,'200905091740540710087','2009050917000071',3,'É¢Ì¨','2009-05-09 17:00:00',30,0,'13771402398','Â½Î°(ÏÈÉú)','','ÖØÒª¿Í»§','£²±¸£±','','','1303','1367','2009-05-09 17:40:54','2009-05-09 17:41:06',0);
INSERT INTO `reservation` VALUES (197,1,74,88,'200905091741010740088','2009050917000074',3,'É¢Ì¨','2009-05-09 17:00:00',40,0,'13806171361','Ç®(ÏÈÉú)','','ÖØÒª¿Í»§','3±¸1¡£¡£¡£Ç®Òæ³¬20ËêÉúÈÕ','','','1303','1367','2009-05-09 17:41:01','2009-05-09 17:41:14',0);
INSERT INTO `reservation` VALUES (198,1,62,106,'200905091946300620106','2009050917000062',3,'É¢Ì¨','2009-05-09 17:00:00',10,0,'£±£³£°£¹£³£°£³£¹£·£³£¹','Öì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1304','2009-05-09 19:46:30','2009-05-09 19:47:06',0);
INSERT INTO `reservation` VALUES (199,1,60,102,'200905091846040600102','2009050917000060',3,'É¢Ì¨','2009-05-09 17:00:00',10,0,'','Àî×Ü(¾­Àí)','','ÖØÒª¿Í»§','','Ëï¾­Àí¶¨','','1303','8868','2009-05-09 18:46:04','2009-05-09 18:46:04',0);
INSERT INTO `reservation` VALUES (201,1,69,103,'200905091859440690103','2009050917000069',3,'É¢Ì¨','2009-05-09 17:00:00',8,0,'','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','','Àî¾­Àí¶¨','','1303','1367','2009-05-09 18:59:44','2009-05-09 18:59:56',0);
INSERT INTO `reservation` VALUES (202,1,50,130,'200905101154470500130','2009051011000050',3,'É¢Ì¨','2009-05-10 11:00:00',13,0,'£±£³£¹£µ£±£µ£·£¹£¶£¶£¹','Ò¦(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-10 11:54:47','2009-05-10 11:54:47',0);
INSERT INTO `reservation` VALUES (203,1,54,122,'200905101129230540122','2009051011000054',3,'É¢Ì¨','2009-05-10 11:00:00',20,0,'¡£¡£','ÕÔ(ÏÈÉú)','£¶£°ËêÉúÈÕ','ÖØÒª¿Í»§','¡££²×ÀºÍ£¸£³£°£¸ÊÇÒ»ÆðµÄ','','','1303','1304','2009-05-10 11:29:23','2009-05-10 11:29:36',0);
INSERT INTO `reservation` VALUES (204,1,58,0,NULL,'2009051011000058',1,'É¢Ì¨','2009-05-10 11:00:00',10,0,'£±£³£µ£¸£µ£°£²£±£¶£±£¶','ºú(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-10 11:04:41','2009-05-10 11:04:51',0);
INSERT INTO `reservation` VALUES (205,1,68,0,NULL,'2009051011000068',1,'É¢Ì¨','2009-05-10 11:00:00',13,0,'13861836339','Îâ(ÏÈÉú)','¸ÖÌú³§','ÖØÒª¿Í»§','ÉúÈÕ','','','1303','','2009-05-10 10:18:02','2009-05-10 15:00:00',0);
INSERT INTO `reservation` VALUES (206,1,71,111,'200905101040460710111','2009051011000071',3,'É¢Ì¨','2009-05-10 11:00:00',120,0,'85062171','Öì(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ333£»335£»337£»339¡£336ÊÇÒ»Æð','','','1303','1367','2009-05-10 10:40:46','2009-05-10 10:40:59',0);
INSERT INTO `reservation` VALUES (207,1,64,116,'200905101101090640116','2009051011000064',3,'É¢Ì¨','2009-05-10 11:00:00',10,0,'','½¯×Ü¶¨(¾­Àí)','ÍÏÀ­»ú³§','ÖØÒª¿Í»§','','','','1303','1308','2009-05-10 11:01:09','2009-05-10 11:01:22',0);
INSERT INTO `reservation` VALUES (208,1,60,0,NULL,'2009051011000060',1,'É¢Ì¨','2009-05-10 11:00:00',4,0,'','Ëï(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','','2009-05-10 11:00:34','2009-05-10 15:00:00',0);
INSERT INTO `reservation` VALUES (209,1,66,121,'200905101128100660121','2009051011000066',3,'É¢Ì¨','2009-05-10 11:00:00',4,0,'','Ëï½ðÃ÷(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','8868','2009-05-10 11:28:10','2009-05-10 11:28:10',0);
INSERT INTO `reservation` VALUES (210,1,52,0,NULL,'2009051017000052',1,'É¢Ì¨','2009-05-10 17:00:00',6,0,'£±£³£·£·£±£°£±£¶£°£·£´','ºú(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-10 17:41:49','2009-05-10 21:00:00',0);
INSERT INTO `reservation` VALUES (211,1,58,162,'200905101757090580162','2009051017000058',3,'É¢Ì¨','2009-05-10 17:00:00',10,0,'£±£³£³£±£±£´£¹£´£·£¹','°²(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','8868','2009-05-10 17:57:09','2009-05-10 17:57:09',0);
INSERT INTO `reservation` VALUES (212,1,59,177,'200905101930080590177','2009051017000059',3,'É¢Ì¨','2009-05-10 17:00:00',5,0,'£±£µ£¹£¹£µ£²£µ£¶£³£±£±','¹¨(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1303','1330','2009-05-10 19:30:08','2009-05-10 19:30:20',0);
INSERT INTO `reservation` VALUES (213,1,60,0,NULL,'2009051017000060',1,'É¢Ì¨','2009-05-10 17:00:00',7,0,'','Îº¶­(¾­Àí)','´ó¼þ','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','','2009-05-10 17:18:02','2009-05-10 21:00:00',0);
INSERT INTO `reservation` VALUES (214,1,62,159,'200905101748590620159','2009051017000062',3,'É¢Ì¨','2009-05-10 17:00:00',10,0,'£±£³£µ£¸£µ£°£¸£¶£¸£°£¹','Ëï(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1315','2009-05-10 17:48:59','2009-05-10 17:49:11',0);
INSERT INTO `reservation` VALUES (215,1,64,158,'200905101747120640158','2009051017000064',3,'É¢Ì¨','2009-05-10 17:00:00',4,0,'','×¯(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1303','8868','2009-05-10 17:47:12','2009-05-10 17:47:12',0);
INSERT INTO `reservation` VALUES (216,1,65,151,'200905101736300650151','2009051017000065',3,'É¢Ì¨','2009-05-10 17:00:00',10,0,'','ºúÀÏ°å(¾­Àí)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1303','1317','2009-05-10 17:36:30','2009-05-10 17:36:43',0);
INSERT INTO `reservation` VALUES (217,1,66,140,'200905101626510660140','2009051017000066',3,'É¢Ì¨','2009-05-10 17:00:00',14,0,'£±£³£¸£¶£±£¸£³£¶£¹£³£¹','Îâ(ÏÈÉú)','¸ÖÌú³§','ÖØÒª¿Í»§','','','','1303','1309','2009-05-10 16:26:51','2009-05-10 16:27:04',0);
INSERT INTO `reservation` VALUES (218,1,68,163,'200905101800300680163','2009051017000068',3,'É¢Ì¨','2009-05-10 17:00:00',4,0,'','Öì(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀÏ°åÄï¶¨','','1303','1312','2009-05-10 18:00:30','2009-05-10 18:00:43',0);
INSERT INTO `reservation` VALUES (219,1,54,0,NULL,'2009051017000054',1,'É¢Ì¨','2009-05-10 17:00:00',6,0,'£±£µ£¹£¹£µ£²£³£°£¶£¸£¶','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','','2009-05-10 17:02:08','2009-05-10 21:00:00',0);
INSERT INTO `reservation` VALUES (220,1,72,147,'200905101729090720147','2009051017000072',3,'É¢Ì¨','2009-05-10 17:00:00',20,0,'','ÂÀ(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ8335ÊÇÒ»ÆðµÄ','','','1303','1367','2009-05-10 17:29:09','2009-05-10 17:29:22',0);
INSERT INTO `reservation` VALUES (221,1,3,0,NULL,'200905101700003',1,'É¢Ì¨','2009-05-10 17:00:00',4,0,'','Íõ×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','','Íõ×Ü¶¨','','1101','','2009-05-10 17:41:35','2009-05-10 17:41:45',0);
INSERT INTO `reservation` VALUES (222,1,50,0,NULL,'2009051017000050',1,'É¢Ì¨','2009-05-10 17:00:00',12,0,'','Äß(ÏÈÉú)','','ÖØÒª¿Í»§','','Â½¾­Àí¶¨','','1303','','2009-05-10 17:03:47','2009-05-10 21:00:00',0);
INSERT INTO `reservation` VALUES (223,1,69,156,'200905101742300690156','2009051017000069',3,'É¢Ì¨','2009-05-10 17:00:00',5,0,'','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶¨','','1303','1333','2009-05-10 17:42:30','2009-05-10 17:42:43',0);
INSERT INTO `reservation` VALUES (224,1,56,166,'200905101812010560166','2009051017000056',3,'É¢Ì¨','2009-05-10 17:00:00',6,0,'15995230686','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1303','1306','2009-05-10 18:12:01','2009-05-10 18:12:40',0);
INSERT INTO `reservation` VALUES (228,1,70,0,NULL,'2009051017000070',1,'É¢Ì¨','2009-05-10 17:00:00',5,0,'','ÈÎÀö»ª¶¨µÄ(¾­Àí)','','ÖØÒª¿Í»§','','','','1303','','2009-05-10 17:48:37','2009-05-10 21:00:00',0);
INSERT INTO `reservation` VALUES (229,1,3,0,NULL,'200905221100003',1,'É¢Ì¨','2009-05-22 11:00:00',4,0,'1123','123(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-21 13:03:40','2009-05-22 15:00:00',0);
INSERT INTO `reservation` VALUES (230,1,6,0,NULL,'200905211700006',1,'É¢Ì¨','2009-05-21 17:00:00',4,0,'22','22(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-21 12:58:31','2009-05-21 21:00:00',0);
INSERT INTO `reservation` VALUES (231,1,4,0,NULL,'200905211700004',1,'É¢Ì¨','2009-05-21 17:00:00',4,0,'22','225(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-21 12:58:38','2009-05-21 21:00:00',0);
INSERT INTO `reservation` VALUES (232,1,53,48,'200905211825400530048','2009052117000053',3,'É¢Ì¨','2009-05-21 17:00:00',10,0,'13771015880','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','8868','2009-05-21 18:25:40','2009-05-21 18:25:40',0);
INSERT INTO `reservation` VALUES (233,1,54,0,NULL,'2009052117000054',1,'É¢Ì¨','2009-05-21 17:00:00',4,0,'£±£³£³£¹£µ£±£¹£¹£²£µ£¸','Äß(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 16:41:50','2009-05-21 21:00:00',0);
INSERT INTO `reservation` VALUES (234,1,56,29,'200905211718080560029','2009052117000056',3,'É¢Ì¨','2009-05-21 17:00:00',10,0,'13961716711','ºú(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','8868','2009-05-21 17:18:08','2009-05-21 17:19:24',0);
INSERT INTO `reservation` VALUES (235,1,62,40,'200905211801470620040','2009052117000062',3,'É¢Ì¨','2009-05-21 17:00:00',10,0,'13861470528','Íõ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','8868','1315','2009-05-21 18:01:47','2009-05-21 18:02:19',0);
INSERT INTO `reservation` VALUES (236,1,65,36,'200905211744470650036','2009052117000065',3,'É¢Ì¨','2009-05-21 17:00:00',15,0,'13771095528','Ëï(Ð¡½ã)','','ÖØÒª¿Í»§','','','','8868','1308','2009-05-21 17:44:47','2009-05-21 17:45:20',0);
INSERT INTO `reservation` VALUES (237,1,58,0,NULL,'2009052111000058',0,'É¢Ì¨','2009-05-21 11:00:00',9,0,'13961758958','³Â(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 15:56:36','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (238,1,61,0,NULL,'2009052111000061',0,'É¢Ì¨','2009-05-21 11:00:00',10,0,'13301515337','³Â    (ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 16:10:20','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (239,1,50,0,NULL,'2009052111000050',0,'É¢Ì¨','2009-05-21 11:00:00',10,0,'13395199258','Äß(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 17:33:45','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (240,1,52,0,NULL,'2009052111000052',0,'É¢Ì¨','2009-05-21 11:00:00',4,0,'15861588130','Áõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 16:54:01','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (241,1,66,0,NULL,'2009052111000066',0,'É¢Ì¨','2009-05-21 11:00:00',4,0,'','³Â×Ü(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü','','1003','','2009-05-21 17:18:20','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (242,1,63,0,NULL,'2009052111000063',0,'É¢Ì¨','2009-05-21 11:00:00',10,0,'','ÐÂ¹â´å (ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-21 17:38:06','2009-05-21 15:00:00',0);
INSERT INTO `reservation` VALUES (243,1,50,73,'200905221128130500073','2009052211000050',3,'É¢Ì¨','2009-05-22 11:00:00',12,0,'85441318','¹ù(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1325','2009-05-22 11:28:13','2009-05-22 11:29:31',0);
INSERT INTO `reservation` VALUES (244,1,75,70,'200905221119280750070','2009052211000075',3,'É¢Ì¨','2009-05-22 11:00:00',24,0,'13914117138','Àî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1322','2009-05-22 11:19:28','2009-05-22 11:20:02',0);
INSERT INTO `reservation` VALUES (245,1,71,0,NULL,'2009052211000071',1,'É¢Ì¨','2009-05-22 11:00:00',40,0,'','ÇÇÖ÷ÈÎ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1101','','2009-05-22 13:07:58','2009-05-22 13:08:27',0);
INSERT INTO `reservation` VALUES (246,1,64,68,'200905221114080640068','2009052211000064',3,'É¢Ì¨','2009-05-22 11:00:00',10,0,'','Ñ¦×Ü(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-22 11:14:08','2009-05-22 11:14:08',0);
INSERT INTO `reservation` VALUES (247,1,65,88,'200905221236070650088','2009052211000065',3,'É¢Ì¨','2009-05-22 11:00:00',12,0,'13906170868','»ª(ÏÈÉú)','·¨¶÷¿Æ¼Ê','ÖØÒª¿Í»§','','','','1003','1329','2009-05-22 12:36:07','2009-05-22 12:36:41',0);
INSERT INTO `reservation` VALUES (248,1,50,104,'200905221809450500104','2009052217000050',3,'É¢Ì¨','2009-05-22 17:00:00',13,0,'88800651','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','ÖÜÀÖÑÔ±¦±¦ÉúÈÕ','','','1003','1325','2009-05-22 18:09:46','2009-05-22 18:11:04',0);
INSERT INTO `reservation` VALUES (249,1,53,101,'200905221801580530101','2009052217000053',3,'É¢Ì¨','2009-05-22 17:00:00',4,0,'13812188585','Öî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-22 18:01:58','2009-05-22 18:03:31',0);
INSERT INTO `reservation` VALUES (250,1,55,0,NULL,'2009052217000055',1,'É¢Ì¨','2009-05-22 17:00:00',8,0,'13951584075','Öì(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-22 19:44:32','2009-05-22 21:00:00',0);
INSERT INTO `reservation` VALUES (251,1,56,114,'200905221944500560114','2009052217000056',3,'É¢Ì¨','2009-05-22 17:00:00',4,0,'13861711097','½¯(Ð¡½ã)','','ÖØÒª¿Í»§','ÂòÃæ½îµÄ','','','1003','1324','2009-05-22 19:44:50','2009-05-22 19:46:09',0);
INSERT INTO `reservation` VALUES (252,1,58,96,'200905221740530580096','2009052217000058',3,'É¢Ì¨','2009-05-22 17:00:00',4,0,'','Â½ËùÀÏÆÅ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1316','2009-05-22 17:40:53','2009-05-22 17:41:28',0);
INSERT INTO `reservation` VALUES (253,1,62,109,'200905221827040620109','2009052217000062',3,'É¢Ì¨','2009-05-22 17:00:00',9,0,'13338111126','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1331','2009-05-22 18:27:04','2009-05-22 18:27:38',0);
INSERT INTO `reservation` VALUES (254,1,66,95,'200905221734520660095','2009052217000066',3,'É¢Ì¨','2009-05-22 17:00:00',12,0,'','¶¡×Ü(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-22 17:34:52','2009-05-22 17:34:52',0);
INSERT INTO `reservation` VALUES (255,1,69,0,NULL,'2009052217000069',1,'É¢Ì¨','2009-05-22 17:00:00',4,0,'13338111126','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-22 19:42:41','2009-05-22 21:00:00',0);
INSERT INTO `reservation` VALUES (256,1,52,0,NULL,'2009052217000052',1,'É¢Ì¨','2009-05-22 17:00:00',4,0,'15951584075','Ðí(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-22 16:49:19','2009-05-22 21:00:00',0);
INSERT INTO `reservation` VALUES (257,1,61,94,'200905221733210610094','2009052217000061',3,'É¢Ì¨','2009-05-22 17:00:00',10,0,'13222939232','»Æ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','1330','2009-05-22 17:33:21','2009-05-22 17:33:56',0);
INSERT INTO `reservation` VALUES (258,1,57,0,NULL,'2009052217000057',1,'É¢Ì¨','2009-05-22 17:00:00',10,0,'13911888202','½­(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','','2009-05-22 19:42:03','2009-05-22 21:00:00',0);
INSERT INTO `reservation` VALUES (259,1,63,100,'200905221759270630100','2009052217000063',3,'É¢Ì¨','2009-05-22 17:00:00',10,0,'','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','ÐÂÁª´ó¶Ó','','','8868','1304','2009-05-22 17:59:27','2009-05-22 18:00:02',0);
INSERT INTO `reservation` VALUES (260,1,73,0,NULL,'2009052211000073',1,'É¢Ì¨','2009-05-22 11:00:00',4,0,'1123456','ÕÅ(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-22 19:49:31','2009-05-22 15:00:00',0);
INSERT INTO `reservation` VALUES (261,1,50,150,'200905231720250500150','2009052317000050',3,'É¢Ì¨','2009-05-23 17:00:00',13,0,'13861857617','³Â(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-23 17:20:25','2009-05-23 17:21:58',0);
INSERT INTO `reservation` VALUES (262,1,54,159,'200905231757550540159','2009052317000054',3,'É¢Ì¨','2009-05-23 17:00:00',6,0,'13921537831','ÑÕ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1349','2009-05-23 17:57:55','2009-05-23 17:59:17',0);
INSERT INTO `reservation` VALUES (263,1,56,155,'200905231749220560155','2009052317000056',3,'É¢Ì¨','2009-05-23 17:00:00',10,0,'13906193730','Ñî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1306','2009-05-23 17:49:22','2009-05-23 17:50:43',0);
INSERT INTO `reservation` VALUES (264,1,58,0,NULL,'2009052317000058',1,'É¢Ì¨','2009-05-23 17:00:00',8,0,'13951500025','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-23 19:01:52','2009-05-23 21:00:00',0);
INSERT INTO `reservation` VALUES (265,1,64,147,'200905231711140640147','2009052317000064',3,'É¢Ì¨','2009-05-23 17:00:00',15,0,'','½«(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-23 17:11:14','2009-05-23 17:12:48',0);
INSERT INTO `reservation` VALUES (266,1,65,0,NULL,'2009052317000065',1,'É¢Ì¨','2009-05-23 17:00:00',15,0,'','½¯(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-23 16:07:42','2009-05-23 21:00:00',0);
INSERT INTO `reservation` VALUES (267,1,68,156,'200905231749460680156','2009052317000068',3,'É¢Ì¨','2009-05-23 17:00:00',12,0,'','³Â(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1312','2009-05-23 17:49:46','2009-05-23 17:50:22',0);
INSERT INTO `reservation` VALUES (268,1,75,164,'200905231819500750164','2009052317000075',3,'É¢Ì¨','2009-05-23 17:00:00',13,0,'13861766443','º«(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-23 18:19:50','2009-05-23 18:19:50',0);
INSERT INTO `reservation` VALUES (269,1,76,148,'200905231714550760148','2009052317000076',3,'É¢Ì¨','2009-05-23 17:00:00',12,0,'15161573312','Áõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1325','2009-05-23 17:14:55','2009-05-23 17:15:31',0);
INSERT INTO `reservation` VALUES (270,1,65,0,NULL,'2009052311000065',0,'É¢Ì¨','2009-05-23 11:00:00',15,0,'13961776087','¹¨(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-23 16:11:57','2009-05-23 15:00:00',0);
INSERT INTO `reservation` VALUES (271,1,62,0,NULL,'2009052311000062',1,'É¢Ì¨','2009-05-23 11:00:00',10,0,'','Ò¶(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-23 19:35:03','2009-05-23 15:00:00',0);
INSERT INTO `reservation` VALUES (272,1,50,205,'200905241138130500205','2009052411000050',3,'É¢Ì¨','2009-05-24 11:00:00',11,0,'15951509398','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1304','2009-05-24 11:38:14','2009-05-24 11:38:50',0);
INSERT INTO `reservation` VALUES (273,1,55,212,'200905241158380550212','2009052411000055',3,'É¢Ì¨','2009-05-24 11:00:00',8,0,'13914138719','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1301','2009-05-24 11:58:38','2009-05-24 11:59:15',0);
INSERT INTO `reservation` VALUES (274,1,60,206,'200905241138250600206','2009052411000060',3,'É¢Ì¨','2009-05-24 11:00:00',12,0,'13601515332','µË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1304','2009-05-24 11:38:25','2009-05-24 11:39:02',0);
INSERT INTO `reservation` VALUES (275,1,72,210,'200905241142110720210','2009052411000072',3,'É¢Ì¨','2009-05-24 11:00:00',20,0,'','¶¡(Ð¡½ã)','ÐÂÁª´ó¶Ó','ÖØÒª¿Í»§','','','','1003','8868','2009-05-24 11:42:11','2009-05-24 11:42:11',0);
INSERT INTO `reservation` VALUES (276,1,75,184,'200905241105380750184','2009052411000075',3,'É¢Ì¨','2009-05-24 11:00:00',20,0,'','Öì(Ð¡½ã)','','ÖØÒª¿Í»§','','ÀîÁá','','1003','1325','2009-05-24 11:05:38','2009-05-24 11:06:15',0);
INSERT INTO `reservation` VALUES (277,1,71,203,'200905241137320710203','2009052411000071',3,'É¢Ì¨','2009-05-24 11:00:00',20,0,'137715211555','Ëï(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1101','2009-05-24 11:37:33','2009-05-24 11:38:04',0);
INSERT INTO `reservation` VALUES (278,1,53,186,'200905241111220530186','2009052411000053',3,'É¢Ì¨','2009-05-24 11:00:00',5,0,'13906188670','Áõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1324','2009-05-24 11:11:22','2009-05-24 11:12:46',0);
INSERT INTO `reservation` VALUES (279,1,64,209,'200905241141450640209','2009052411000064',3,'É¢Ì¨','2009-05-24 11:00:00',12,0,'','ºã·á(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-24 11:41:45','2009-05-24 11:41:45',0);
INSERT INTO `reservation` VALUES (280,1,56,215,'200905241621070560215','2009052417000056',3,'É¢Ì¨','2009-05-24 17:00:00',10,0,'13906175689','ÎÂ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1306','2009-05-24 16:21:07','2009-05-24 16:21:45',0);
INSERT INTO `reservation` VALUES (281,1,60,227,'200905241738540600227','2009052417000060',3,'É¢Ì¨','2009-05-24 17:00:00',7,0,'','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-24 17:38:54','2009-05-24 17:38:54',0);
INSERT INTO `reservation` VALUES (282,1,66,223,'200905241729380660223','2009052417000066',3,'É¢Ì¨','2009-05-24 17:00:00',12,0,'','Ôø(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1309','2009-05-24 17:29:38','2009-05-24 17:30:15',0);
INSERT INTO `reservation` VALUES (283,1,72,221,'200905241725360720221','2009052417000072',3,'É¢Ì¨','2009-05-24 17:00:00',20,0,'','Öì(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-24 17:25:37','2009-05-24 17:25:36',0);
INSERT INTO `reservation` VALUES (284,1,75,237,'200905241804080750237','2009052417000075',3,'É¢Ì¨','2009-05-24 17:00:00',24,0,'13951589655','Ñï(ÏÈÉú)','','ÖØÒª¿Í»§','12ÈËÒ»×À','','','1003','1325','2009-05-24 18:04:08','2009-05-24 18:04:45',0);
INSERT INTO `reservation` VALUES (285,1,71,239,'200905241807280710239','2009052417000071',3,'É¢Ì¨','2009-05-24 17:00:00',50,0,'','Àî(ÏÈÉú)','','ÖØÒª¿Í»§','Îå±¸Ò»','','','1003','1390','2009-05-24 18:07:28','2009-05-24 18:08:05',0);
INSERT INTO `reservation` VALUES (286,1,62,219,'200905241725120620219','2009052417000062',3,'É¢Ì¨','2009-05-24 17:00:00',10,0,'','Ñ¦(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-24 17:25:12','2009-05-24 17:25:12',0);
INSERT INTO `reservation` VALUES (287,1,58,0,NULL,'2009052517000058',1,'É¢Ì¨','2009-05-25 17:00:00',12,0,'13511655871','Ëï(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1350','','2009-05-25 17:35:01','2009-05-25 17:35:40',0);
INSERT INTO `reservation` VALUES (288,1,64,28,'200905251739150640028','2009052517000064',3,'É¢Ì¨','2009-05-25 17:00:00',15,0,'','³Â(ÏÈÉú)','','ÖØÒª¿Í»§','ÈýÃ«¶¨','','','1003','1322','2009-05-25 17:39:15','2009-05-25 17:39:54',0);
INSERT INTO `reservation` VALUES (289,1,66,0,NULL,'2009052517000066',1,'É¢Ì¨','2009-05-25 17:00:00',12,0,'','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-25 17:36:02','2009-05-25 17:36:35',0);
INSERT INTO `reservation` VALUES (290,1,54,0,NULL,'2009052517000054',1,'É¢Ì¨','2009-05-25 17:00:00',6,0,'','Æ½(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-25 16:56:16','2009-05-25 21:00:00',0);
INSERT INTO `reservation` VALUES (292,1,57,0,NULL,'2009052517000057',1,'É¢Ì¨','2009-05-25 17:00:00',4,0,'','Æ½(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-25 16:58:55','2009-05-25 21:00:00',0);
INSERT INTO `reservation` VALUES (293,1,50,0,NULL,'2009052611000050',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','Â½×Ü¶¨(¾­Àí)','','ÖØÒª¿Í»§','ÄÏ³¤ÇøÀÍ¶¯¾Ö','','','1301','','2009-05-26 15:56:45','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (294,1,54,0,NULL,'2009052611000054',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','»ÆÏÈÉú(ÏÈÉú)','','ÖØÒª¿Í»§','³ÂÓ±´ú¶¨','','','1301','','2009-05-26 15:57:48','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (295,1,56,0,NULL,'2009052611000056',0,'É¢Ì¨','2009-05-26 11:00:00',7,0,'','Ð¡ÀÏ°å¶¨(¾­Àí)','','ÖØÒª¿Í»§','Ð¡ÀÏ°å¶©','','','1301','','2009-05-26 15:58:50','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (296,1,64,0,NULL,'2009052611000064',0,'É¢Ì¨','2009-05-26 11:00:00',16,0,'','ËïÏÈÉú(¾­Àí)','','ÖØÒª¿Í»§','±¸£²Æ¿º£Ö®À¶¡¡¡¡¡¡£¶µãÀ´µã²Ë','','','1301','','2009-05-26 16:00:35','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (297,1,65,0,NULL,'2009052611000065',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','³ÂÏÈÉú(¾­Àí)','','ÖØÒª¿Í»§','ºã¿­´ó¾Æµê','ÈýÃ«¶©','','1301','','2009-05-26 16:01:41','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (298,1,66,0,NULL,'2009052611000066',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','ºã·á(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«¶©','','1301','','2009-05-26 17:11:46','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (299,1,68,0,NULL,'2009052611000068',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','Â½ÏÈÉú(¾­Àí)','ÄÏÈª','ÖØÒª¿Í»§','','ÈýÃ«¶©','','1301','','2009-05-26 16:03:33','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (300,1,72,0,NULL,'2009052611000072',0,'É¢Ì¨','2009-05-26 11:00:00',20,0,'','ÖìÏÈÉú(¾­Àí)','','ÖØÒª¿Í»§','','','','1301','','2009-05-26 16:04:05','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (301,1,75,0,NULL,'2009052611000075',0,'É¢Ì¨','2009-05-26 11:00:00',20,0,'','»ÆÐ¡½ã(¾­Àí)','','ÖØÒª¿Í»§','','','','1301','','2009-05-26 16:04:42','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (302,1,58,0,NULL,'2009052611000058',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','ÐÂ¹â´å(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«¶©','','1301','','2009-05-26 16:34:36','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (303,1,53,0,NULL,'2009052611000053',0,'É¢Ì¨','2009-05-26 11:00:00',7,0,'','ÕÅÏÈÉú(¾­Àí)','','ÖØÒª¿Í»§','','','','1301','','2009-05-26 16:47:56','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (304,1,67,0,NULL,'2009052611000067',0,'É¢Ì¨','2009-05-26 11:00:00',0,0,'','ÖìÐË¶È(¾­Àí)','','ÖØÒª¿Í»§','','','','1301','','2009-05-26 17:11:25','2009-05-26 15:00:00',0);
INSERT INTO `reservation` VALUES (305,1,50,64,'200905271738160500064','2009052717000050',3,'É¢Ì¨','2009-05-27 17:00:00',12,0,'','³Â(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1327','2009-05-27 17:38:16','2009-05-27 17:39:48',0);
INSERT INTO `reservation` VALUES (306,1,52,0,NULL,'2009052717000052',1,'É¢Ì¨','2009-05-27 17:00:00',6,0,'13656192308','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-27 17:52:00','2009-05-27 17:52:37',0);
INSERT INTO `reservation` VALUES (307,1,53,63,'200905271737250530063','2009052717000053',3,'É¢Ì¨','2009-05-27 17:00:00',5,0,'13606185432','Ê©(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1306','2009-05-27 17:37:25','2009-05-27 17:38:58',0);
INSERT INTO `reservation` VALUES (308,1,56,0,NULL,'2009052717000056',1,'É¢Ì¨','2009-05-27 17:00:00',10,0,'13812271601','Éò(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-27 19:34:25','2009-05-27 19:35:02',0);
INSERT INTO `reservation` VALUES (309,1,59,0,NULL,'2009052717000059',1,'É¢Ì¨','2009-05-27 17:00:00',10,0,'13093095389','³Â(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1386','','2009-05-27 18:04:29','2009-05-27 18:05:11',0);
INSERT INTO `reservation` VALUES (310,1,68,0,NULL,'2009052717000068',1,'É¢Ì¨','2009-05-27 17:00:00',12,0,'','Í¯(ÏÈÉú)','','ÖØÒª¿Í»§','Ëï×Ü¶©','','','1003','','2009-05-27 16:28:40','2009-05-27 21:00:00',0);
INSERT INTO `reservation` VALUES (311,1,72,0,NULL,'2009052717000072',1,'É¢Ì¨','2009-05-27 17:00:00',12,0,'13961764151','Óà(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-27 18:54:16','2009-05-27 21:00:00',0);
INSERT INTO `reservation` VALUES (312,1,75,76,'200905271823500750076','2009052717000075',3,'É¢Ì¨','2009-05-27 17:00:00',20,0,'13961830696','²ñ(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ339Ò»Æð2×À','','','1003','5601','2009-05-27 18:23:50','2009-05-27 18:24:32',0);
INSERT INTO `reservation` VALUES (313,1,60,0,NULL,'2009052717000060',1,'É¢Ì¨','2009-05-27 17:00:00',10,0,'13861770528','Íõ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-27 17:35:51','2009-05-27 17:36:28',0);
INSERT INTO `reservation` VALUES (314,1,54,0,NULL,'2009052717000054',1,'É¢Ì¨','2009-05-27 17:00:00',6,0,'13506176490','·¶(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-27 18:11:08','2009-05-27 18:11:44',0);
INSERT INTO `reservation` VALUES (315,1,64,0,NULL,'2009052717000064',1,'É¢Ì¨','2009-05-27 17:00:00',4,0,'','Í¯(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-27 17:39:54','2009-05-27 17:40:30',0);
INSERT INTO `reservation` VALUES (316,1,63,77,'200905271824420630077','2009052717000063',3,'É¢Ì¨','2009-05-27 17:00:00',10,0,'','Í¯(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ318Ò»Æð','','','1003','5601','2009-05-27 18:24:42','2009-05-27 18:25:24',0);
INSERT INTO `reservation` VALUES (317,1,73,82,'200905271925430730082','2009052717000073',3,'É¢Ì¨','2009-05-27 17:00:00',12,0,'15052298887','»Æ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1333','2009-05-27 19:25:43','2009-05-27 19:26:25',0);
INSERT INTO `reservation` VALUES (318,1,53,0,NULL,'2009052711000053',1,'É¢Ì¨','2009-05-27 11:00:00',6,0,'82532011','ÕÅ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 09:11:06','2009-05-27 15:00:00',0);
INSERT INTO `reservation` VALUES (319,1,56,0,NULL,'2009052711000056',1,'É¢Ì¨','2009-05-27 11:00:00',8,0,'13961717953','Ê¢(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 09:11:12','2009-05-27 15:00:00',0);
INSERT INTO `reservation` VALUES (320,1,64,0,NULL,'2009052711000064',1,'É¢Ì¨','2009-05-27 11:00:00',14,0,'','Â½(¾­Àí)','','ÖØÒª¿Í»§','1000±ê×¼ÒÑ½»','','','1003','','2009-05-28 09:11:25','2009-05-27 15:00:00',0);
INSERT INTO `reservation` VALUES (321,1,68,0,NULL,'2009052711000068',1,'É¢Ì¨','2009-05-27 11:00:00',1,0,'','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','ºã·á','','','1003','','2009-05-28 09:11:18','2009-05-27 15:00:00',0);
INSERT INTO `reservation` VALUES (322,1,75,0,NULL,'2009052711000075',1,'É¢Ì¨','2009-05-27 11:00:00',24,0,'13861720058','¹Ë(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ339Ò»ÆðµÄ12ÈËÒ»×À','','','1003','','2009-05-28 09:11:34','2009-05-27 15:00:00',0);
INSERT INTO `reservation` VALUES (326,1,50,0,NULL,'2009052817000050',1,'É¢Ì¨','2009-05-28 17:00:00',12,0,'13706191620','Ðì(ÏÈÉú)','ÖÐÔ¶¹«Ë¾','ÖØÒª¿Í»§','','','','1003','','2009-05-28 09:22:21','2009-05-28 21:00:00',0);
INSERT INTO `reservation` VALUES (327,1,53,0,NULL,'2009052811000053',1,'É¢Ì¨','2009-05-28 11:00:00',6,0,'82532011','ÕÅ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 12:32:59','2009-05-28 15:00:00',0);
INSERT INTO `reservation` VALUES (328,1,56,0,NULL,'2009052811000056',1,'É¢Ì¨','2009-05-28 11:00:00',8,0,'13961717953','Ê¢(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1306','','2009-05-28 12:05:44','2009-05-28 12:07:18',0);
INSERT INTO `reservation` VALUES (329,1,64,96,'200905281120000640096','2009052811000064',3,'É¢Ì¨','2009-05-28 11:00:00',14,0,'','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','1000Ôª±ê×¼ÒÑ½»','','','1003','1329','2009-05-28 11:20:00','2009-05-28 11:20:42',0);
INSERT INTO `reservation` VALUES (330,1,68,88,'200905281057080680088','2009052811000068',3,'É¢Ì¨','2009-05-28 11:00:00',12,0,'','Íõ×Ü(¾­Àí)','ºã·á','ÖØÒª¿Í»§','','','','1003','1309','2009-05-28 10:57:08','2009-05-28 10:57:51',0);
INSERT INTO `reservation` VALUES (331,1,75,101,'200905281131390750101','2009052811000075',3,'É¢Ì¨','2009-05-28 11:00:00',24,0,'13861720058','¹Ë(ÏÈÉú)','','ÖØÒª¿Í»§','ºÍ339Ò»Æð12ÈË1×À','','','1003','1304','2009-05-28 11:31:39','2009-05-28 11:32:22',0);
INSERT INTO `reservation` VALUES (336,1,52,103,'200905281133080520103','2009052811000052',3,'É¢Ì¨','2009-05-28 11:00:00',5,0,'13093083786','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1324','2009-05-28 11:33:08','2009-05-28 11:34:43',0);
INSERT INTO `reservation` VALUES (337,1,66,0,NULL,'2009052811000066',1,'É¢Ì¨','2009-05-28 11:00:00',12,0,'','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶©','','1101','','2009-05-28 11:38:41','2009-05-28 11:39:18',0);
INSERT INTO `reservation` VALUES (339,1,50,87,'200905281054090500087','2009052811000050',3,'É¢Ì¨','2009-05-28 11:00:00',12,0,'','½¯(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1327','2009-05-28 10:54:10','2009-05-28 10:55:44',0);
INSERT INTO `reservation` VALUES (340,1,1,0,NULL,'200905281700001',1,'É¢Ì¨','2009-05-28 17:00:00',4,0,'','¡¡njhgj¡¡¡¡(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 10:39:40','2009-05-28 21:00:00',0);
INSERT INTO `reservation` VALUES (350,1,53,140,'200905281716580530140','2009052817000053',3,'É¢Ì¨','2009-05-28 17:00:00',7,0,'13606174365','Ç®(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1324','2009-05-28 17:16:59','2009-05-28 17:18:33',0);
INSERT INTO `reservation` VALUES (356,1,54,141,'200905281720220540141','2009052817000054',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'13665114844','³Â(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1349','2009-05-28 17:20:22','2009-05-28 17:21:57',0);
INSERT INTO `reservation` VALUES (357,1,55,150,'200905281742500550150','2009052817000055',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'13961807242','·®(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1002','2009-05-28 17:42:50','2009-05-28 17:44:25',0);
INSERT INTO `reservation` VALUES (358,1,56,0,NULL,'2009052817000056',1,'É¢Ì¨','2009-05-28 17:00:00',8,0,'13921135002','Àî(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1306','','2009-05-28 17:06:25','2009-05-28 17:08:00',0);
INSERT INTO `reservation` VALUES (359,1,57,0,NULL,'2009052817000057',1,'É¢Ì¨','2009-05-28 17:00:00',10,0,'1555008777','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1101','','2009-05-28 18:43:56','2009-05-28 18:44:33',0);
INSERT INTO `reservation` VALUES (361,1,58,171,'200905281818390580171','2009052817000058',3,'É¢Ì¨','2009-05-28 17:00:00',8,0,'','ÖÜ³ø(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1349','2009-05-28 18:18:39','2009-05-28 18:19:23',0);
INSERT INTO `reservation` VALUES (362,1,59,164,'200905281803570590164','2009052817000059',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'13951589375','Îâ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 18:03:57','2009-05-28 18:03:57',0);
INSERT INTO `reservation` VALUES (363,1,61,0,NULL,'2009052817000061',1,'É¢Ì¨','2009-05-28 17:00:00',12,0,'13812518317','Åí(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1386','','2009-05-28 17:14:53','2009-05-28 17:15:37',0);
INSERT INTO `reservation` VALUES (364,1,62,159,'200905281758010620159','2009052817000062',3,'É¢Ì¨','2009-05-28 17:00:00',6,0,'','¹Ë(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1315','2009-05-28 17:58:01','2009-05-28 17:58:45',0);
INSERT INTO `reservation` VALUES (365,1,64,131,'200905281648480640131','2009052817000064',3,'É¢Ì¨','2009-05-28 17:00:00',16,0,'','ÖìÀÏ°å(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 16:48:48','2009-05-28 16:48:48',0);
INSERT INTO `reservation` VALUES (366,1,65,165,'200905281804170650165','2009052817000065',3,'É¢Ì¨','2009-05-28 17:00:00',12,0,'13951507679','²Ü(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 18:04:17','2009-05-28 18:04:17',0);
INSERT INTO `reservation` VALUES (367,1,66,132,'200905281649010660132','2009052817000066',3,'É¢Ì¨','2009-05-28 17:00:00',15,0,'13861838985','ÍõÐÐ³¤(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 16:49:01','2009-05-28 16:49:01',0);
INSERT INTO `reservation` VALUES (368,1,67,136,'200905281710390670136','2009052817000067',3,'É¢Ì¨','2009-05-28 17:00:00',13,0,'','ÎÄ(ÏÈÉú)','ÖþÂ·»úÐµ³§','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 17:10:39','2009-05-28 17:10:39',0);
INSERT INTO `reservation` VALUES (369,1,68,144,'200905281725390680144','2009052817000068',3,'É¢Ì¨','2009-05-28 17:00:00',12,0,'','´ï(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1003','8868','2009-05-28 17:25:39','2009-05-28 17:25:39',0);
INSERT INTO `reservation` VALUES (370,1,69,168,'200905281809370690168','2009052817000069',3,'É¢Ì¨','2009-05-28 17:00:00',6,0,'13801513261','Ò¶(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1333','2009-05-28 18:09:37','2009-05-28 18:10:21',0);
INSERT INTO `reservation` VALUES (371,1,75,143,'200905281725310750143','2009052817000075',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'','¹ËÊé¼Ç(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1316','2009-05-28 17:25:31','2009-05-28 17:26:15',0);
INSERT INTO `reservation` VALUES (372,1,76,180,'200905281840110760180','2009052817000076',3,'É¢Ì¨','2009-05-28 17:00:00',13,0,'85061297','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-28 18:40:11','2009-05-28 18:40:11',0);
INSERT INTO `reservation` VALUES (373,1,71,0,NULL,'2009052817000071',1,'É¢Ì¨','2009-05-28 17:00:00',30,0,'','ÓÈ(ÏÈÉú)','','ÖØÒª¿Í»§','','Â½×Ü¶©','','1003','','2009-05-28 15:49:25','2009-05-28 21:00:00',0);
INSERT INTO `reservation` VALUES (377,1,79,173,'200905281819310790173','2009052817000079',3,'É¢Ì¨','2009-05-28 17:00:00',22,0,'','Â½(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1006','2009-05-28 18:19:31','2009-05-28 18:21:06',0);
INSERT INTO `reservation` VALUES (381,1,52,0,NULL,'2009052817000052',1,'É¢Ì¨','2009-05-28 17:00:00',4,0,'','£±£²£³(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 16:00:35','2009-05-28 21:00:00',0);
INSERT INTO `reservation` VALUES (383,1,50,0,NULL,'2009052818000050',1,'É¢Ì¨','2009-05-28 18:00:00',4,0,'','123(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-28 16:00:27','2009-05-28 22:00:00',0);
INSERT INTO `reservation` VALUES (399,1,60,128,'200905281624110600128','2009052817000060',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'13656185959','³£(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','8868','2009-05-28 16:24:11','2009-05-28 16:24:11',0);
INSERT INTO `reservation` VALUES (405,1,50,0,NULL,'2009052819000050',1,'É¢Ì¨','2009-05-28 19:00:00',4,0,'','£±£²£³(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-05-28 16:13:04','2009-05-28 23:00:00',0);
INSERT INTO `reservation` VALUES (406,1,50,155,'200905281754470500155','2009052817010050',3,'É¢Ì¨','2009-05-28 17:01:00',10,0,'','Ðí(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','1388','2009-05-28 17:54:47','2009-05-28 17:56:22',0);
INSERT INTO `reservation` VALUES (407,1,71,158,'200905281757040710158','2009052817580071',3,'É¢Ì¨','2009-05-28 17:58:00',36,0,'','ÓÉ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','1333','2009-05-28 17:57:04','2009-05-28 17:57:48',0);
INSERT INTO `reservation` VALUES (408,1,70,167,'200905281808590700167','2009052817000070',3,'É¢Ì¨','2009-05-28 17:00:00',10,0,'13306171819','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','8868','2009-05-28 18:08:59','2009-05-28 18:08:59',0);
INSERT INTO `reservation` VALUES (410,1,52,145,'200905281729100520145','2009052818000052',3,'É¢Ì¨','2009-05-28 18:00:00',6,0,'13861760058','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','1304','2009-05-28 17:29:10','2009-05-28 17:29:54',0);
INSERT INTO `reservation` VALUES (411,1,53,0,NULL,'2009052911000053',1,'É¢Ì¨','2009-05-29 11:00:00',8,0,'','Ð¡±¾(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-05-29 11:22:04','2009-05-29 11:22:42',0);
INSERT INTO `reservation` VALUES (412,1,64,201,'200905291125090640201','2009052911000064',3,'É¢Ì¨','2009-05-29 11:00:00',15,0,'','Ð¡Äï¾Ë(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','1006','2009-05-29 11:25:09','2009-05-29 11:26:47',0);
INSERT INTO `reservation` VALUES (413,1,66,185,'200905291044490660185','2009052911000066',3,'É¢Ì¨','2009-05-29 11:00:00',15,0,'','ÊÙ»á¼Æ(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','1309','2009-05-29 10:44:49','2009-05-29 10:45:33',0);
INSERT INTO `reservation` VALUES (414,1,68,186,'200905291045120680186','2009052911000068',3,'É¢Ì¨','2009-05-29 11:00:00',12,0,'','ÖÜ(ÏÈÉú)','Îý¸Ö','ÖØÒª¿Í»§','','','','8868','1309','2009-05-29 10:45:12','2009-05-29 10:45:56',0);
INSERT INTO `reservation` VALUES (415,1,71,184,'200905291013450710184','2009052911000071',3,'É¢Ì¨','2009-05-29 11:00:00',50,0,'','Ëï½¡(ÏÈÉú)','','ÖØÒª¿Í»§','5±¸1','','','8868','1318','2009-05-29 10:13:45','2009-05-29 10:14:29',0);
INSERT INTO `reservation` VALUES (416,1,58,0,NULL,'2009052911000058',1,'É¢Ì¨','2009-05-29 11:00:00',9,0,'','ÀîÐÂ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-29 09:04:30','2009-05-29 15:00:00',0);
INSERT INTO `reservation` VALUES (417,1,57,0,NULL,'2009052917000057',1,'É¢Ì¨','2009-05-29 17:00:00',8,0,'13093083786','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-28 19:43:49','2009-05-29 21:00:00',0);
INSERT INTO `reservation` VALUES (418,1,46,203,'200905291128410460203','2009052911000046',3,'É¢Ì¨','2009-05-29 11:00:00',60,0,'','ÌÆ(ÏÈÉú)','','ÖØÒª¿Í»§','6±¸1','','','8868','8868','2009-05-29 11:28:41','2009-05-29 11:39:32',0);
INSERT INTO `reservation` VALUES (419,1,56,0,NULL,'2009052917000056',1,'É¢Ì¨','2009-05-29 17:00:00',10,0,'13801510600','ÁÖ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-29 09:03:57','2009-05-29 21:00:00',0);
INSERT INTO `reservation` VALUES (421,1,57,0,NULL,'2009052916000057',1,'É¢Ì¨','2009-05-29 16:00:00',8,0,'13093083786','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-29 09:03:50','2009-05-29 20:00:00',0);
INSERT INTO `reservation` VALUES (423,1,58,0,NULL,'2009052910000058',1,'É¢Ì¨','2009-05-29 10:00:00',9,0,'','Àî(ÏÈÉú)','','ÖØÒª¿Í»§','','Ð¡ÀÏ°å¶©','','1350','','2009-05-29 10:51:50','2009-05-29 10:52:34',0);
INSERT INTO `reservation` VALUES (425,1,56,0,NULL,'2009052915000056',1,'É¢Ì¨','2009-05-29 15:00:00',10,0,'13801510600','ÁÖ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1306','','2009-05-29 12:12:21','2009-05-29 12:13:58',0);
INSERT INTO `reservation` VALUES (426,1,57,0,NULL,'2009052915000057',1,'É¢Ì¨','2009-05-29 15:00:00',8,0,'13093083786','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','8868','','2009-05-29 12:39:15','2009-05-29 19:00:00',0);
INSERT INTO `reservation` VALUES (427,1,59,0,NULL,'2009052915000059',1,'É¢Ì¨','2009-05-29 15:00:00',9,0,'13806170237','Ðì(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-29 12:39:45','2009-05-29 12:40:23',0);
INSERT INTO `reservation` VALUES (428,1,64,212,'200905291216460640212','2009052915000064',3,'É¢Ì¨','2009-05-29 15:00:00',15,0,'85805293','×¯(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','8868','1304','2009-05-29 12:16:46','2009-05-29 12:17:30',0);
INSERT INTO `reservation` VALUES (429,1,66,0,NULL,'2009052915000066',1,'É¢Ì¨','2009-05-29 15:00:00',16,0,'','ÕÔÊé¼Ç(ÏÈÉú)','ÐÂ¹â´å','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1102','','2009-05-29 12:40:28','2009-05-29 12:41:07',0);
INSERT INTO `reservation` VALUES (430,1,68,0,NULL,'2009052915000068',1,'É¢Ì¨','2009-05-29 15:00:00',4,0,'','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1102','','2009-05-29 12:40:19','2009-05-29 12:40:58',0);
INSERT INTO `reservation` VALUES (431,1,75,0,NULL,'2009052915000075',1,'É¢Ì¨','2009-05-29 15:00:00',12,0,'13812077863','Çñ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','8868','','2009-05-29 12:39:23','2009-05-29 19:00:00',0);
INSERT INTO `reservation` VALUES (432,1,53,237,'200905291744490530237','2009052917000053',3,'É¢Ì¨','2009-05-29 17:00:00',9,0,'137015116560','³Â(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1324','2009-05-29 17:44:49','2009-05-29 17:46:28',0);
INSERT INTO `reservation` VALUES (433,1,55,244,'200905291802450550244','2009052917000055',3,'É¢Ì¨','2009-05-29 17:00:00',8,0,'13656190997','½­(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1304','2009-05-29 18:02:45','2009-05-29 18:03:30',0);
INSERT INTO `reservation` VALUES (436,1,57,0,NULL,'2009052919000057',1,'É¢Ì¨','2009-05-29 19:00:00',8,0,'13093083786','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-29 18:58:13','2009-05-29 23:00:00',0);
INSERT INTO `reservation` VALUES (437,1,59,225,'200905291618150590225','2009052917000059',3,'É¢Ì¨','2009-05-29 17:00:00',9,0,'13806170237','Ðì(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1330','2009-05-29 16:18:15','2009-05-29 16:19:00',0);
INSERT INTO `reservation` VALUES (438,1,64,239,'200905291749230640239','2009052917000064',3,'É¢Ì¨','2009-05-29 17:00:00',15,0,'85805293','×¯(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','5553','2009-05-29 17:49:23','2009-05-29 17:50:08',0);
INSERT INTO `reservation` VALUES (439,1,66,226,'200905291631170660226','2009052917000066',3,'É¢Ì¨','2009-05-29 17:00:00',16,0,'','ÕÔÊé¼Ç(ÏÈÉú)','','ÖØÒª¿Í»§','','Ëï×Ü','','1003','1309','2009-05-29 16:31:17','2009-05-29 16:32:02',0);
INSERT INTO `reservation` VALUES (440,1,67,229,'200905291647290670229','2009052917000067',3,'É¢Ì¨','2009-05-29 17:00:00',14,0,'','´ï(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1003','1328','2009-05-29 16:47:29','2009-05-29 16:48:14',0);
INSERT INTO `reservation` VALUES (441,1,68,232,'200905291710280680232','2009052917000068',3,'É¢Ì¨','2009-05-29 17:00:00',10,0,'','Â½(ÏÈÉú)','','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1003','1304','2009-05-29 17:10:28','2009-05-29 17:11:13',0);
INSERT INTO `reservation` VALUES (442,1,75,0,NULL,'2009052917000075',1,'É¢Ì¨','2009-05-29 17:00:00',12,0,'13812077863','Çñ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','','2009-05-29 18:58:30','2009-05-29 21:00:00',0);
INSERT INTO `reservation` VALUES (443,1,61,241,'200905291752550610241','2009052917000061',3,'É¢Ì¨','2009-05-29 17:00:00',8,0,'13665162106','Ð¤(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1315','2009-05-29 17:52:56','2009-05-29 17:53:41',0);
INSERT INTO `reservation` VALUES (444,1,58,0,NULL,'2009052917000058',1,'É¢Ì¨','2009-05-29 17:00:00',1,0,'','ÅË(ÏÈÉú)','','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1350','','2009-05-29 17:40:55','2009-05-29 17:41:40',0);
INSERT INTO `reservation` VALUES (445,1,50,306,'200905301745130500306','2009053017000050',3,'É¢Ì¨','2009-05-30 17:00:00',15,0,'13961765940','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1327','2009-05-30 17:45:13','2009-05-30 17:46:55',0);
INSERT INTO `reservation` VALUES (446,1,52,303,'200905301737230520303','2009053017000052',3,'É¢Ì¨','2009-05-30 17:00:00',5,0,'13812518317','Åí(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1349','2009-05-30 17:37:23','2009-05-30 17:39:05',0);
INSERT INTO `reservation` VALUES (447,1,53,0,NULL,'2009053017000053',1,'É¢Ì¨','2009-05-30 17:00:00',6,0,'13771070042','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1350','','2009-05-30 16:25:50','2009-05-30 16:27:31',0);
INSERT INTO `reservation` VALUES (448,1,55,297,'200905301728190550297','2009053017000055',3,'É¢Ì¨','2009-05-30 17:00:00',8,0,'13901517966','¼¾(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1304','2009-05-30 17:28:19','2009-05-30 17:29:06',0);
INSERT INTO `reservation` VALUES (449,1,56,317,'200905301830120560317','2009053017000056',3,'É¢Ì¨','2009-05-30 17:00:00',10,0,'','¶¡(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1304','2009-05-30 18:30:12','2009-05-30 18:31:54',0);
INSERT INTO `reservation` VALUES (450,1,58,0,NULL,'2009053017000058',1,'É¢Ì¨','2009-05-30 17:00:00',10,0,'15895338119','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-30 16:15:54','2009-05-30 21:00:00',0);
INSERT INTO `reservation` VALUES (451,1,59,310,'200905301804020590310','2009053017000059',3,'É¢Ì¨','2009-05-30 17:00:00',8,0,'13906188503','Áõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-05-30 18:04:02','2009-05-30 18:04:01',0);
INSERT INTO `reservation` VALUES (452,1,63,0,NULL,'2009053017000063',1,'É¢Ì¨','2009-05-30 17:00:00',8,0,'13706191291','ÃÏ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1006','','2009-05-30 19:02:45','2009-05-30 19:03:32',0);
INSERT INTO `reservation` VALUES (453,1,64,288,'200905301654420640288','2009053017000064',3,'É¢Ì¨','2009-05-30 17:00:00',12,0,'','ÕÔÖ÷ÈÎ(ÏÈÉú)','ÐÂÁª','ÖØÒª¿Í»§','','','','1003','1308','2009-05-30 16:54:42','2009-05-30 16:55:29',0);
INSERT INTO `reservation` VALUES (454,1,65,0,NULL,'2009053017000065',1,'É¢Ì¨','2009-05-30 17:00:00',14,0,'13812288893','Ê·(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1006','','2009-05-30 17:33:23','2009-05-30 17:34:10',0);
INSERT INTO `reservation` VALUES (455,1,66,293,'200905301709510660293','2009053017000066',3,'É¢Ì¨','2009-05-30 17:00:00',13,0,'','ÕÔ(ÏÈÉú)','ÐÂ¹â´å','ÖØÒª¿Í»§','','','','1003','1006','2009-05-30 17:09:51','2009-05-30 17:10:38',0);
INSERT INTO `reservation` VALUES (456,1,68,291,'200905301702340680291','2009053017000068',3,'É¢Ì¨','2009-05-30 17:00:00',12,0,'','ÖÜÀÏ°å(¾­Àí)','','ÖØÒª¿Í»§','','½¯×Ü¶©','','1003','1328','2009-05-30 17:02:34','2009-05-30 17:03:21',0);
INSERT INTO `reservation` VALUES (457,1,72,0,NULL,'2009053017000072',1,'É¢Ì¨','2009-05-30 17:00:00',13,0,'13601482329','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-05-30 18:32:16','2009-05-30 21:00:00',0);
INSERT INTO `reservation` VALUES (458,1,75,314,'200905301818040750314','2009053017000075',3,'É¢Ì¨','2009-05-30 17:00:00',20,0,'','ÌÆ¶©(¾­Àí)','','ÖØÒª¿Í»§','ºÍ339Ò»Æð','','','1003','1316','2009-05-30 18:18:04','2009-05-30 18:18:51',0);
INSERT INTO `reservation` VALUES (459,1,71,286,'200905301638070710286','2009053017000071',3,'É¢Ì¨','2009-05-30 17:00:00',50,0,'','Ñï(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1309','2009-05-30 16:38:07','2009-05-30 16:38:54',0);
INSERT INTO `reservation` VALUES (460,1,73,0,NULL,'2009053017000073',1,'É¢Ì¨','2009-05-30 17:00:00',4,0,'','Â½×Ü¶©(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-05-30 18:46:57','2009-05-30 21:00:00',0);
INSERT INTO `reservation` VALUES (461,1,67,319,'200905301837240670319','2009053017000067',3,'É¢Ì¨','2009-05-30 17:00:00',13,0,'15895338119','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1328','2009-05-30 18:37:24','2009-05-30 18:38:11',0);
INSERT INTO `reservation` VALUES (462,1,56,0,NULL,'2009053117000056',1,'É¢Ì¨','2009-05-31 17:00:00',8,0,'13656190997','ÕÅ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-05-31 17:38:46','2009-05-31 17:39:28',0);
INSERT INTO `reservation` VALUES (463,1,58,0,NULL,'2009053117000058',1,'É¢Ì¨','2009-05-31 17:00:00',10,0,'','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','','Â½×Ü¶©','','1003','','2009-05-31 17:44:39','2009-05-31 21:00:00',0);
INSERT INTO `reservation` VALUES (464,1,62,0,NULL,'2009053117000062',1,'É¢Ì¨','2009-05-31 17:00:00',4,0,'','´óÁ¦ÆðÖØ(¾­Àí)','','ÖØÒª¿Í»§','','½¯¾­Àí¶©','','1102','','2009-05-31 17:20:58','2009-05-31 17:21:40',0);
INSERT INTO `reservation` VALUES (465,1,68,0,NULL,'2009053117000068',1,'É¢Ì¨','2009-05-31 17:00:00',12,0,'','¹Ë(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯¶©','','1102','','2009-05-31 17:17:13','2009-05-31 17:17:54',0);
INSERT INTO `reservation` VALUES (466,1,75,27,'200905311729410750027','2009053117000075',3,'É¢Ì¨','2009-05-31 17:00:00',20,0,'','¹ù(ÏÈÉú)','','ÖØÒª¿Í»§','¹ù¼Ò³Ï±¦±¦Ò»ÖÜËêºÍ339Ò»Æð','','','1003','1304','2009-05-31 17:29:41','2009-05-31 17:30:29',0);
INSERT INTO `reservation` VALUES (467,1,64,0,NULL,'2009053117000064',1,'É¢Ì¨','2009-05-31 17:00:00',12,0,'','Ðì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-31 17:13:15','2009-05-31 17:13:57',0);
INSERT INTO `reservation` VALUES (468,1,60,0,NULL,'2009053117000060',1,'É¢Ì¨','2009-05-31 17:00:00',8,0,'','ÖÓ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-05-31 17:43:00','2009-05-31 17:43:42',0);
INSERT INTO `reservation` VALUES (469,1,52,0,NULL,'2009060217000052',1,'É¢Ì¨','2009-06-02 17:00:00',1,0,'','Ëï×Ü¶©(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 18:09:58','2009-06-02 18:10:43',0);
INSERT INTO `reservation` VALUES (470,1,53,28,'200906021801530530028','2009060217000053',3,'É¢Ì¨','2009-06-02 17:00:00',7,0,'','Îº(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯ÏþÀ¼¶©','','1003','1324','2009-06-02 18:01:53','2009-06-02 18:03:45',0);
INSERT INTO `reservation` VALUES (471,1,56,0,NULL,'2009060217000056',1,'É¢Ì¨','2009-06-02 17:00:00',8,0,'13921197371','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 17:29:26','2009-06-02 17:30:10',0);
INSERT INTO `reservation` VALUES (472,1,61,0,NULL,'2009060217000061',1,'É¢Ì¨','2009-06-02 17:00:00',10,0,'18921278305','±«(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 18:02:20','2009-06-02 18:03:04',0);
INSERT INTO `reservation` VALUES (473,1,64,15,'200906021659470640015','2009060217000064',3,'É¢Ì¨','2009-06-02 17:00:00',13,0,'13033503391','¶¡(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1301','2009-06-02 16:59:47','2009-06-02 17:00:37',0);
INSERT INTO `reservation` VALUES (474,1,66,22,'200906021750050660022','2009060217000066',3,'É¢Ì¨','2009-06-02 17:00:00',12,0,'','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1388','2009-06-02 17:50:05','2009-06-02 17:50:56',0);
INSERT INTO `reservation` VALUES (475,1,68,0,NULL,'2009060217000068',1,'É¢Ì¨','2009-06-02 17:00:00',12,0,'','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 18:32:25','2009-06-02 18:33:10',0);
INSERT INTO `reservation` VALUES (476,1,76,13,'200906021627490760013','2009060217000076',3,'É¢Ì¨','2009-06-02 17:00:00',12,0,'13771052277','½¯(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1386','2009-06-02 16:27:49','2009-06-02 16:28:40',0);
INSERT INTO `reservation` VALUES (477,1,75,0,NULL,'2009060217000075',1,'É¢Ì¨','2009-06-02 17:00:00',12,0,'13771052277','½¯(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 17:58:08','2009-06-02 17:58:53',0);
INSERT INTO `reservation` VALUES (478,1,69,0,NULL,'2009060217000069',1,'É¢Ì¨','2009-06-02 17:00:00',6,0,'13585091108','³Â(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1102','','2009-06-02 17:38:53','2009-06-02 17:39:37',0);
INSERT INTO `reservation` VALUES (479,1,66,41,'200906031805110660041','2009060317000066',3,'É¢Ì¨','2009-06-03 17:00:00',11,0,'','¹ËÅ®Ê¿(¾­Àí)','','ÖØÒª¿Í»§','','ÈýÃ«','','1301','1330','2009-06-03 18:05:11','2009-06-03 18:06:05',0);
INSERT INTO `reservation` VALUES (480,1,53,26,'200906031711330530026','2009060317000053',3,'É¢Ì¨','2009-06-03 17:00:00',1,0,'','Îº×Ü(¾­Àí)','','ÖØÒª¿Í»§','','½¯Ð¡À¼','','1301','1380','2009-06-03 17:11:33','2009-06-03 17:12:27',0);
INSERT INTO `reservation` VALUES (481,1,56,0,NULL,'2009060317000056',1,'É¢Ì¨','2009-06-03 17:00:00',7,0,'','ÖÜÐ¡½ã(¾­Àí)','','ÖØÒª¿Í»§','','','','1102','','2009-06-03 17:28:03','2009-06-03 17:28:49',0);
INSERT INTO `reservation` VALUES (482,1,68,0,NULL,'2009060311000068',1,'É¢Ì¨','2009-06-03 11:00:00',1,0,'','ÕÔÊé¼Ç(¾­Àí)','','ÖØÒª¿Í»§','ÐÂ¹â´å','ÈýÃ«','','1301','','2009-06-03 17:07:20','2009-06-03 15:00:00',0);
INSERT INTO `reservation` VALUES (483,1,64,0,NULL,'2009060317000064',1,'É¢Ì¨','2009-06-03 17:00:00',1,0,'','ÉÛÊé¼Ç(¾­Àí)','ÐÂ¹â´å','ÖØÒª¿Í»§','','ÈýÃ«','','1102','','2009-06-03 17:27:44','2009-06-03 17:28:30',0);
INSERT INTO `reservation` VALUES (484,1,53,0,NULL,'2009060417000053',1,'É¢Ì¨','2009-06-04 17:00:00',6,0,'13801515010','½ð(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-06-04 16:19:22','2009-06-04 21:00:00',0);
INSERT INTO `reservation` VALUES (485,1,54,97,'200906041810560540097','2009060417000054',3,'É¢Ì¨','2009-06-04 17:00:00',7,0,'','»Æ(Ð¡½ã)','ÂÞÊÏÖÐÑÇ','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1003','1349','2009-06-04 18:10:56','2009-06-04 18:12:54',0);
INSERT INTO `reservation` VALUES (486,1,56,0,NULL,'2009060417000056',1,'É¢Ì¨','2009-06-04 17:00:00',8,0,'13812296958','ÕÅ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','','2009-06-04 15:37:52','2009-06-04 21:00:00',0);
INSERT INTO `reservation` VALUES (487,1,58,0,NULL,'2009060417000058',1,'É¢Ì¨','2009-06-04 17:00:00',10,0,'85416055','¶«º£ìÑÔì(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-06-04 17:09:04','2009-06-04 21:00:00',0);
INSERT INTO `reservation` VALUES (488,1,65,82,'200906041719410650082','2009060417000065',3,'É¢Ì¨','2009-06-04 17:00:00',12,0,'','Ðí(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-06-04 17:19:41','2009-06-04 17:19:41',0);
INSERT INTO `reservation` VALUES (489,1,68,80,'200906041713540680080','2009060417000068',3,'É¢Ì¨','2009-06-04 17:00:00',12,0,'13814252145','Öì(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1330','2009-06-04 17:13:54','2009-06-04 17:14:49',0);
INSERT INTO `reservation` VALUES (490,1,72,104,'200906041827210720104','2009060417000072',3,'É¢Ì¨','2009-06-04 17:00:00',11,0,'15852706335','Áõ(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','1304','2009-06-04 18:27:21','2009-06-04 18:28:16',0);
INSERT INTO `reservation` VALUES (491,1,71,78,'200906041702090710078','2009060417000071',3,'É¢Ì¨','2009-06-04 17:00:00',30,0,'','ÎÖ¿É¹«Ë¾(¾­Àí)','','ÖØÒª¿Í»§','3×À²Ëµ¥ÒÑ½»','','','1003','1333','2009-06-04 17:02:09','2009-06-04 17:03:04',0);
INSERT INTO `reservation` VALUES (492,1,59,77,'200906041640590590077','2009060417000059',3,'É¢Ì¨','2009-06-04 17:00:00',7,0,'1322939232','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1329','2009-06-04 16:40:59','2009-06-04 16:41:55',0);
INSERT INTO `reservation` VALUES (493,1,67,89,'200906041743190670089','2009060417000067',3,'É¢Ì¨','2009-06-04 17:00:00',10,0,'','Áõ(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1329','2009-06-04 17:43:19','2009-06-04 17:44:14',0);
INSERT INTO `reservation` VALUES (494,1,60,0,NULL,'2009060417000060',1,'É¢Ì¨','2009-06-04 17:00:00',10,0,'13801515010','½ð(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1350','','2009-06-04 18:24:33','2009-06-04 18:25:28',0);
INSERT INTO `reservation` VALUES (495,1,62,79,'200906041703060620079','2009060417000062',3,'É¢Ì¨','2009-06-04 17:00:00',10,0,'','ÑîÀÏÊ¦(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1327','2009-06-04 17:03:06','2009-06-04 17:04:02',0);
INSERT INTO `reservation` VALUES (496,1,61,0,NULL,'2009060417000061',1,'É¢Ì¨','2009-06-04 17:00:00',10,0,'13921281978','Ò¦(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','','2009-06-04 16:53:01','2009-06-04 21:00:00',0);
INSERT INTO `reservation` VALUES (498,1,53,98,'200906041810590530098','2009060418000053',3,'É¢Ì¨','2009-06-04 18:00:00',7,0,'13961755530','ãÚ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-06-04 18:10:59','2009-06-04 18:10:59',0);
INSERT INTO `reservation` VALUES (499,1,64,0,NULL,'2009060417000064',1,'É¢Ì¨','2009-06-04 17:00:00',4,0,'','¶«º£ìÑÔì(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-06-04 18:26:55','2009-06-04 21:00:00',0);
INSERT INTO `reservation` VALUES (500,1,50,39,'200906061737360500039','2009060617000050',3,'É¢Ì¨','2009-06-06 17:00:00',15,0,'85412353','Ó¡(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','8868','2009-06-06 17:37:36','2009-06-06 17:37:36',0);
INSERT INTO `reservation` VALUES (501,1,53,48,'200906061755340530048','2009060617000053',3,'É¢Ì¨','2009-06-06 17:00:00',6,0,'13812017675','Ö£Ö÷ÈÎ(¾­Àí)','´ó¼þ','ÖØÒª¿Í»§','','','','1003','8868','2009-06-06 17:55:34','2009-06-06 17:55:34',0);
INSERT INTO `reservation` VALUES (502,1,56,64,'200906061828120560064','2009060617000056',3,'É¢Ì¨','2009-06-06 17:00:00',10,0,'','¹ù(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀîÆ¼¶©','','1003','8868','2009-06-06 18:28:12','2009-06-06 18:28:12',0);
INSERT INTO `reservation` VALUES (503,1,58,0,NULL,'2009060617000058',1,'É¢Ì¨','2009-06-06 17:00:00',10,0,'','ÖÜ(Ð¡½ã)','','ÖØÒª¿Í»§','','ÀîÁá¶©','','1350','','2009-06-06 17:42:12','2009-06-06 17:43:10',0);
INSERT INTO `reservation` VALUES (504,1,59,60,'200906061817260590060','2009060617000059',3,'É¢Ì¨','2009-06-06 17:00:00',10,0,'13771132809','½¯(Ð¡½ã)','','ÖØÒª¿Í»§','','','','1003','8868','2009-06-06 18:17:26','2009-06-06 18:17:26',0);
INSERT INTO `reservation` VALUES (505,1,61,0,NULL,'2009060617000061',1,'É¢Ì¨','2009-06-06 17:00:00',10,0,'1386130641','Ò¦(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1102','','2009-06-06 17:52:58','2009-06-06 17:53:49',0);
INSERT INTO `reservation` VALUES (506,1,62,62,'200906061820480620062','2009060617000062',3,'É¢Ì¨','2009-06-06 17:00:00',10,0,'','ÖÜ½Ü¶©(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','1315','2009-06-06 18:20:48','2009-06-06 18:21:47',0);
INSERT INTO `reservation` VALUES (507,1,64,32,'200906061643540640032','2009060617000064',3,'É¢Ì¨','2009-06-06 17:00:00',1,0,'','Íõ×Ü(¾­Àí)','ºã·á','ÖØÒª¿Í»§','','','','1003','8868','2009-06-06 16:43:54','2009-06-06 16:43:54',0);
INSERT INTO `reservation` VALUES (508,1,67,0,NULL,'2009060617000067',1,'É¢Ì¨','2009-06-06 17:00:00',14,0,'','Â½¾­Àí(¾­Àí)','','ÖØÒª¿Í»§','','','','1003','','2009-06-06 14:53:22','2009-06-06 21:00:00',0);
INSERT INTO `reservation` VALUES (509,1,68,0,NULL,'2009060617000068',1,'É¢Ì¨','2009-06-06 17:00:00',1,0,'','Ê©(ÏÈÉú)','','ÖØÒª¿Í»§','','½¯×Ü¶©','','1003','','2009-06-06 15:29:35','2009-06-06 21:00:00',0);
INSERT INTO `reservation` VALUES (510,1,72,43,'200906061744550720043','2009060617000072',3,'É¢Ì¨','2009-06-06 17:00:00',12,0,'13093029799','Ã«(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1331','2009-06-06 17:44:55','2009-06-06 17:45:53',0);
INSERT INTO `reservation` VALUES (511,1,75,36,'200906061715150750036','2009060617000075',3,'É¢Ì¨','2009-06-06 17:00:00',1,0,'13706176875','¹Ë(ÏÈÉú)','','ÖØÒª¿Í»§','339Ò»ÆðµÄ','','','1003','8868','2009-06-06 17:15:15','2009-06-06 17:15:15',0);
INSERT INTO `reservation` VALUES (512,1,71,0,NULL,'2009060617000071',1,'É¢Ì¨','2009-06-06 17:00:00',1,0,'','Áõ(Ð¡½ã)','Ò¶ÂÖ3×À','ÖØÒª¿Í»§','','','','1102','','2009-06-06 18:38:07','2009-06-06 18:38:58',0);
INSERT INTO `reservation` VALUES (513,1,74,58,'200906061812040740058','2009060617000074',3,'É¢Ì¨','2009-06-06 17:00:00',1,0,'','Áª·¢µç»ú(¾­Àí)','','ÖØÒª¿Í»§','±ê×¼µ¥700ÒÑ½»','½¯×Ü¶©','','1003','1331','2009-06-06 18:12:04','2009-06-06 18:13:02',0);
INSERT INTO `reservation` VALUES (514,1,55,35,'200906061714480550035','2009060617000055',3,'É¢Ì¨','2009-06-06 17:00:00',8,0,'','ÌÕ(ÏÈÉú)','','ÖØÒª¿Í»§','','ÀîÆ¼¶©','','1003','8868','2009-06-06 17:14:48','2009-06-06 17:14:48',0);
INSERT INTO `reservation` VALUES (516,1,68,0,NULL,'2009060616000068',1,'É¢Ì¨','2009-06-06 16:00:00',1,0,'','ÉÛÊé¼Ç(¾­Àí)','ÐÂ¹â´å','ÖØÒª¿Í»§','','ÀîÁá¶©','','1003','','2009-06-06 15:56:33','2009-06-06 20:00:00',0);
INSERT INTO `reservation` VALUES (517,1,50,0,NULL,'2009061817000050',1,'É¢Ì¨','2009-06-18 17:00:00',1,0,'','Íõ(ÏÈÉú)','','ÖØÒª¿Í»§','','Ëï×Ü¶©','','1003','','2009-06-18 18:38:59','2009-06-18 21:00:00',0);
INSERT INTO `reservation` VALUES (518,1,54,0,NULL,'2009061817000054',1,'É¢Ì¨','2009-06-18 17:00:00',1,0,'','¹ËÅ®Ê¿(¾­Àí)','ÐÂÁª','ÖØÒª¿Í»§','','','','1102','','2009-06-18 17:22:42','2009-06-18 17:23:49',0);
INSERT INTO `reservation` VALUES (519,1,58,163,'200906181752040580163','2009061817000058',3,'É¢Ì¨','2009-06-18 17:00:00',1,0,'','Ç®(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1301','1350','2009-06-18 17:52:04','2009-06-18 17:53:22',0);
INSERT INTO `reservation` VALUES (520,1,59,162,'200906181749570590162','2009061817000059',3,'É¢Ì¨','2009-06-18 17:00:00',10,0,'13306196526','»ª(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1301','1330','2009-06-18 17:49:57','2009-06-18 17:51:15',0);
INSERT INTO `reservation` VALUES (521,1,61,177,'200906181844000610177','2009061817000061',3,'É¢Ì¨','2009-06-18 17:00:00',8,0,'13961899457','´÷(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1301','1304','2009-06-18 18:44:00','2009-06-18 18:45:19',0);
INSERT INTO `reservation` VALUES (522,1,65,158,'200906181733380650158','2009061817000065',3,'É¢Ì¨','2009-06-18 17:00:00',15,0,'','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1301','1308','2009-06-18 17:33:39','2009-06-18 17:34:57',0);
INSERT INTO `reservation` VALUES (523,1,66,155,'200906181721150660155','2009061817000066',3,'É¢Ì¨','2009-06-18 17:00:00',15,0,'13906180672','Ëï×Ü¶©(¾­Àí)','','ÖØÒª¿Í»§','','','','1301','8868','2009-06-18 17:21:15','2009-06-18 17:22:34',0);
INSERT INTO `reservation` VALUES (524,1,0,157,'200906181728550750157','2009061817000075',3,'É¢Ì¨','2009-06-18 17:00:00',12,0,'13093000705','Ñî(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1301','1301','2009-06-18 17:28:55','2009-06-18 21:00:00',0);
INSERT INTO `reservation` VALUES (525,1,56,0,NULL,'2009061817000056',1,'É¢Ì¨','2009-06-18 17:00:00',8,0,'','ÀîÁá¶©(¾­Àí)','','ÖØÒª¿Í»§','','','','8868','','2009-06-18 17:26:26','2009-06-18 17:27:45',0);
INSERT INTO `reservation` VALUES (526,1,60,178,'200906181848590600178','2009061817000060',3,'É¢Ì¨','2009-06-18 17:00:00',1,0,'','ÖÜ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1304','2009-06-18 18:48:59','2009-06-18 18:50:17',0);
INSERT INTO `reservation` VALUES (527,1,0,156,'200906181725380670156','2009061817000067',3,'É¢Ì¨','2009-06-18 17:00:00',0,0,'','ÕÔ(ÏÈÉú)','','ÖØÒª¿Í»§','','','','1003','1003','2009-06-18 17:28:38','2009-06-18 21:00:00',0);
INSERT INTO `reservation` VALUES (528,1,45,0,NULL,'2012061911000045',0,'É¢Ì¨','2012-06-19 11:00:00',4,0,'2222','22(¾­Àí)','222','ÖØÒª¿Í»§','51,','22','222','Ì·¿¡·å','','2012-06-19 10:44:15','2012-06-19 15:00:00',0);
INSERT INTO `reservation` VALUES (529,1,90,0,NULL,'2012061911000090',0,'É¢Ì¨','2012-06-19 11:00:00',4,0,'222','22(¾­Àí)','','ÖØÒª¿Í»§','61,','','','Ì·¿¡·å','','2012-06-19 10:44:31','2012-06-19 15:00:00',0);
INSERT INTO `reservation` VALUES (530,1,98,0,NULL,'2012061911000098',0,'É¢Ì¨','2012-06-19 11:00:00',4,0,'222','22(¾­Àí)','2222','ÖØÒª¿Í»§','71,','22','2222','Ì·¿¡·å','','2012-06-19 10:44:45','2012-06-19 15:00:00',0);
INSERT INTO `reservation` VALUES (531,1,35,0,NULL,'2012061911000035',0,'É¢Ì¨','2012-06-19 11:00:00',4,0,'1','11(¾­Àí)','','ÖØÒª¿Í»§','40,','','','Ì·¿¡·å','','2012-06-19 11:20:11','2012-06-19 15:00:00',0);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `restaurant` (
  `number` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(64) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `postscript` varchar(64) DEFAULT NULL,
  `or_id_start` bigint(20) unsigned NOT NULL DEFAULT '1000',
  `carry_on` int(10) unsigned NOT NULL DEFAULT '0',
  `carry_on_1` varchar(32) DEFAULT NULL,
  `carry_time_begin_1` time DEFAULT NULL,
  `carry_time_end_1` time DEFAULT NULL,
  `carry_on_2` varchar(32) DEFAULT NULL,
  `carry_time_begin_2` time DEFAULT NULL,
  `carry_time_end_2` time DEFAULT NULL,
  `carry_on_3` varchar(32) DEFAULT NULL,
  `carry_time_begin_3` time DEFAULT NULL,
  `carry_time_end_3` time DEFAULT NULL,
  `carry_on_4` varchar(32) DEFAULT NULL,
  `carry_time_begin_4` time DEFAULT NULL,
  `carry_time_end_4` time DEFAULT NULL,
  `fiscal_period` date DEFAULT NULL,
  `current_period` int(11) DEFAULT '1',
  `is_auto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`number`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (1,'ÎÞÎýÈó»ª´ó¾Æµê','ÎÞÎýºþ±õÇøÇåêÌÂ·168ºÅ','0510-85188588  85616777','ÎÞÎýÈó»ª´ó¾Æµê',1,2,'ÖÐ°à','09:00:00','15:00:00','Íí°à','15:01:00','22:30:00','','00:00:00','00:00:00','','00:00:00','04:00:00','2012-09-24',1,0);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sort`
--

DROP TABLE IF EXISTS `sort`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `postscript` varchar(64) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `postscript` (`postscript`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sort`
--

LOCK TABLES `sort` WRITE;
/*!40000 ALTER TABLE `sort` DISABLE KEYS */;
/*!40000 ALTER TABLE `sort` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sort_item`
--

DROP TABLE IF EXISTS `sort_item`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sort_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_id` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sort_item`
--

LOCK TABLES `sort_item` WRITE;
/*!40000 ALTER TABLE `sort_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sort_item` ENABLE KEYS */;
UNLOCK TABLES;
