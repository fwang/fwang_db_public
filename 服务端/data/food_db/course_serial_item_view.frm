TYPE=VIEW
query=select `a`.`serial_id` AS `serial_id`,`a`.`is_switch` AS `is_switch`,`a`.`course_id` AS `course_id`,`a`.`count` AS `count`,`c`.`number` AS `number`,`c`.`name` AS `name`,`c`.`name_en` AS `name_en`,`c`.`ab` AS `ab`,`b`.`type` AS `type`,`c`.`type` AS `dept`,`b`.`price1` AS `price1`,`b`.`price2` AS `price2`,`b`.`price3` AS `price3`,`b`.`price4` AS `price4`,`b`.`price5` AS `price5`,`b`.`price6` AS `price6`,`b`.`price7` AS `price7`,`b`.`price8` AS `price8`,`b`.`unit` AS `unit`,`b`.`sale_unit` AS `sale_unit`,`c`.`is_sellout` AS `is_sellout`,`b`.`is_serial` AS `is_serial`,`b`.`is_modif_price` AS `is_modif_price`,`b`.`is_dish` AS `is_dish`,`c`.`sellout_cause` AS `sellout_cause`,`c`.`sellout_quantity` AS `sellout_quantity` from `food_db`.`course_serial_item` `a` join `food_db`.`course_unit_price` `b` join `food_db`.`course` `c` where ((`a`.`course_id` = `b`.`course_id`) and (`c`.`id` = `b`.`course_id`))
md5=831281a993cd48ed868892c53184010c
updatable=1
algorithm=0
definer_user=root
definer_host=localhost
suid=2
with_check_option=0
revision=1
timestamp=2012-07-24 16:29:51
create-version=1
source=SELECT a.serial_id,a.is_switch,a.course_id,a.count,c.number,c.name,c.name_en,c.ab,b.type,c.type as dept,b.price1,b.price2,b.price3,b.price4,b.price5,b.price6,b.price7,b.price8,			b.unit,b.sale_unit,c.is_sellout,b.is_serial,b.is_modif_price,b.is_dish,c.sellout_cause,c.sellout_quantity			FROM food_db.course_serial_item as a, food_db.course_unit_price as b, food_db.course as c 			where a.course_id=b.course_id and c.id=b.course_id
client_cs_name=latin1
connection_cl_name=latin1_swedish_ci
view_body_utf8=SELECT a.serial_id,a.is_switch,a.course_id,a.count,c.number,c.name,c.name_en,c.ab,b.type,c.type as dept,b.price1,b.price2,b.price3,b.price4,b.price5,b.price6,b.price7,b.price8,			b.unit,b.sale_unit,c.is_sellout,b.is_serial,b.is_modif_price,b.is_dish,c.sellout_cause,c.sellout_quantity			FROM food_db.course_serial_item as a, food_db.course_unit_price as b, food_db.course as c 			where a.course_id=b.course_id and c.id=b.course_id
