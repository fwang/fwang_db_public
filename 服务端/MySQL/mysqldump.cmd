echo off
del data\food_db.sql
del data\history_db.sql
mysql\bin\mysqldump -u root --opt --extended-insert=false --triggers -R --hex-blob --single-transaction  food_db >data\food_db.sql
mysql\bin\mysqldump -u root --opt --extended-insert=false --triggers -R --hex-blob --single-transaction  history_db >data\history_db.sql
echo 备份数据库脚本完成!
